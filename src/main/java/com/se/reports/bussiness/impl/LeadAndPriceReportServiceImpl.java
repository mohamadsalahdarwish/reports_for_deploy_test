package com.se.reports.bussiness.impl;

import java.io.ByteArrayInputStream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.se.reports.apiclients.bom.BOMAPIClient;
import com.se.reports.apiclients.price.PriceClient;
import com.se.reports.bussiness.LeadAndPriceReportService;
import com.se.reports.download.excel.DownloadExcelReport;
import com.se.reports.download.util.ReportsUtil;
import com.se.reports.dto.BomDTO;
import com.se.reports.dto.ReportDTO;
import com.se.reports.dto.ReportsSeviceParamsDTO;
import com.streaming.excel.silicon.excel.builder.ExcelStreaming;

@Service
public class LeadAndPriceReportServiceImpl extends GlobalAbstractReportsImpl implements LeadAndPriceReportService  {

	final static Logger logger = Logger.getLogger(LeadAndPriceReportServiceImpl.class);
	
	@Autowired
	BOMAPIClient bomApiClient;
	
	@Autowired
	DownloadExcelReport downloadExcelReport;
	
	@Autowired
	PriceClient priceClient;
	
	@Override
	public ReportDTO updateSEReportData(ReportDTO retReportDTO) {
		return priceClient.getPriceLeadReportData(retReportDTO);
	}

	@Override
	public ResponseEntity<?> downloadReportData(ReportsSeviceParamsDTO reportsSeviceParamsDTO){

		InputStreamResource response = null;		
		
		try {		
			
			ReportDTO reportDTO = getReportData(reportsSeviceParamsDTO);
			if(reportDTO == null || reportDTO.getBomDataDTO() == null){
				logger.error("reportDTO or bomDTO is null");
				return new ResponseEntity<>("ERROR", HttpStatus.OK);
			}
			
			String bomName = ReportsUtil.replaceSpecialCharacterWithUnderscore((reportDTO.getBomDataDTO().getBomName() != null ? reportDTO.getBomDataDTO().getBomName().trim() : ""));			
			
			ExcelStreaming excelStreaming = new ExcelStreaming();			
			
			ByteArrayInputStream result = downloadExcelReport.drawReport(excelStreaming, reportDTO, reportsSeviceParamsDTO);	
			
			if(result == null)
			{
				return new ResponseEntity<>("ERROR", HttpStatus.OK);
			}
			response = new InputStreamResource(result);			
			
			return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + bomName + "_Price_Lead_Time_Report.xlsx").contentType(MediaType.valueOf("application/vnd.ms-excel")).body(response);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR", e);
		}	
		
		return new ResponseEntity<>("ERROR", HttpStatus.OK);
	}

}

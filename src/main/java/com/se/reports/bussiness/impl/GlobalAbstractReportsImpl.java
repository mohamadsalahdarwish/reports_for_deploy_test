package com.se.reports.bussiness.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.se.reports.apiclients.bom.BOMAPIClient;
import com.se.reports.dto.BomDTO;
import com.se.reports.dto.ReportDTO;
import com.se.reports.dto.ReportsSeviceParamsDTO;
import com.se.reports.dto.ValidationDTO;
import com.se.reports.utils.Constants;

public abstract class GlobalAbstractReportsImpl  {

	@Autowired
	BOMAPIClient bomapiClient;
	final static Logger logger = Logger.getLogger(GlobalAbstractReportsImpl.class);
	public ReportDTO getReportData(ReportsSeviceParamsDTO reportsSeviceParamsDTO) {		
		long allReportCallingTime = System.currentTimeMillis();
		ReportDTO reportDTO = getBomData(reportsSeviceParamsDTO);

		if (reportDTO.getErrorMessage() != null && reportDTO.getErrorMessage().isEmpty()) {
			updateSEReportData(reportDTO);
		}
		reportDTO.getServiceTimeDTO().setAllReportServiceTime(System.currentTimeMillis()-allReportCallingTime);
		return reportDTO;

	};

	public abstract ReportDTO updateSEReportData(ReportDTO retReportDTO);

	public ReportDTO getBomData(ReportsSeviceParamsDTO reportsSeviceParamsDTO) {
		long bomDataAndValidationTime = System.currentTimeMillis();
		ReportDTO retReportDTO = new ReportDTO();
		BomDTO bomDTO = bomapiClient.getBomData(reportsSeviceParamsDTO);
		ValidationDTO validationDTO = validateBomDTO(bomDTO);
		if (validationDTO.isValid()) {
			retReportDTO.setBomDataDTO(bomDTO);
		} else {
			retReportDTO.getErrorMessage().addAll(validationDTO.getErrorMessages());
		}
		retReportDTO.getServiceTimeDTO().setBomServiceTime(System.currentTimeMillis()-bomDataAndValidationTime);
		return retReportDTO;
	}

	// use to validate response from service
	private ValidationDTO validateBomDTO(BomDTO bomDTO) {
		ValidationDTO validationDTO = new ValidationDTO();
		if (bomDTO != null && bomDTO.getErrorMessage() != null && !bomDTO.getErrorMessage().isEmpty()) {
			validationDTO.setValid(false);
			validationDTO.getErrorMessages().add(Constants.BOM_VALIDATION_ERROR_MESSAGE+ bomDTO.getErrorMessage());
		} else if (bomDTO != null) {
			validationDTO.setValid(true);
		} else {
			validationDTO.setValid(false);
			validationDTO.getErrorMessages().add(Constants.NULL_FROM_BOM_SERVICE);
		}
		return validationDTO;
	}
}

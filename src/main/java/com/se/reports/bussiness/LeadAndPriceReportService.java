package com.se.reports.bussiness;

import org.springframework.http.ResponseEntity;

import com.se.reports.dto.ReportDTO;
import com.se.reports.dto.ReportsSeviceParamsDTO;

public interface LeadAndPriceReportService{
	
	public ReportDTO getReportData(ReportsSeviceParamsDTO reportsSeviceParamsDTO);

	public ResponseEntity<?> downloadReportData(ReportsSeviceParamsDTO reportsSeviceParamsDTO);
}

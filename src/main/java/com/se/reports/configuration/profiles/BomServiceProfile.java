package com.se.reports.configuration.profiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import com.se.reports.configuration.beans.BomServiceConfiguration;


public class BomServiceProfile {

	@Autowired
	BomServiceConfiguration bomServiceConfiguration;

	@Profile("dev")
	@Bean
	public BomServiceConfiguration devDatabaseConnection() {
		return bomServiceConfiguration;
	}

	@Profile("test")
	@Bean
	public BomServiceConfiguration testDatabaseConnection() {
		return bomServiceConfiguration;
	}

	@Profile("prod")
	@Bean
	public BomServiceConfiguration prodDatabaseConnection() {
		return bomServiceConfiguration;
	}
}

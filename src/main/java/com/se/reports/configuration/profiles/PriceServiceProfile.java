package com.se.reports.configuration.profiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import com.se.reports.configuration.beans.PriceServiceConfiguration;


public class PriceServiceProfile {
	@Autowired
	PriceServiceConfiguration priceServiceConfiguration;

	@Profile("dev")
	@Bean
	public PriceServiceConfiguration devDatabaseConnection() {
		return priceServiceConfiguration;
	}

	@Profile("test")
	@Bean
	public PriceServiceConfiguration testDatabaseConnection() {
		return priceServiceConfiguration;
	}

	@Profile("prod")
	@Bean
	public PriceServiceConfiguration prodDatabaseConnection() {
		return priceServiceConfiguration;
	}
}

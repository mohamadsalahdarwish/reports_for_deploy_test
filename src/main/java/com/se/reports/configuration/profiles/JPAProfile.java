package com.se.reports.configuration.profiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import com.se.reports.configuration.beans.JPAConfiguration;


public class JPAProfile {

	@Autowired
	JPAConfiguration jpaConfiguration;

	@Profile("dev")
	@Bean
	public JPAConfiguration devDatabaseConnection() {
		return jpaConfiguration;
	}

	@Profile("test")
	@Bean
	public JPAConfiguration testDatabaseConnection() {
		return jpaConfiguration;
	}

	@Profile("prod")
	@Bean
	public JPAConfiguration prodDatabaseConnection() {
		return jpaConfiguration;
	}
}

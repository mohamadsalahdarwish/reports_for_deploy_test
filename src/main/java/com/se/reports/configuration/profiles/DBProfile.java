/**
 * 
 */
/**
 * @author mohammad_darwish
 *
 */
package com.se.reports.configuration.profiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import com.se.reports.configuration.beans.DBConfiguration;


public class DBProfile {

	@Autowired
	DBConfiguration dbConfiguration;

	@Profile("dev")
	@Bean
	public DBConfiguration devDatabaseConnection() {
		return dbConfiguration;
	}

	@Profile("test")
	@Bean
	public DBConfiguration testDatabaseConnection() {
		return dbConfiguration;
	}

	@Profile("prod")
	@Bean
	public DBConfiguration prodDatabaseConnection() {
		return dbConfiguration;
	}
}
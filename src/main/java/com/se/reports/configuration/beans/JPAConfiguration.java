package com.se.reports.configuration.beans;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("spring.jpa.properties.hibernate")
public class JPAConfiguration {

	private String showSql;
	private String useSql;
	private String formatSql;
	private final Batch batch = new Batch();

	public String getShowSql() {
		return showSql;
	}

	public void setShowSql(String showSql) {
		this.showSql = showSql;
	}

	public String getUseSql() {
		return useSql;
	}

	public void setUseSql(String useSql) {
		this.useSql = useSql;
	}

	public String getFormatSql() {
		return formatSql;
	}

	public void setFormatSql(String formatSql) {
		this.formatSql = formatSql;
	}

	public Batch getBatch() {
		return batch;
	}

	public static class Batch {
		private String size;

		public String getSize() {
			return size;
		}

		public void setSize(String size) {
			this.size = size;
		}

	}
}

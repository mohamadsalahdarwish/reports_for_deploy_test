package com.se.reports.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 
 * @author khalid_galal
 *
 */
public class Utils {

	final static Logger logger = Logger.getLogger(Utils.class);

	/**
	 * @author abdelrhim
	 */
	public static String getProperty(String filePath, String property) {
		// logger.info("getProperty filePath : "+filePath+" & property : "+property+"");

		String value = "";
		BufferedReader in = null;
		try {
			// folder = hasValue(folder)?folder:"session";

			final Properties props = new Properties();

			File fileDir = new File(filePath);

			in = new BufferedReader(new InputStreamReader(new FileInputStream(
					fileDir), "UTF8"));

			props.load(in);

			value = props.getProperty(property).trim();

			// logger.info("getProperty filePath : "+filePath+" & property : "+property+" & value :  "
			// + value);
		} catch (Exception ex) {
			logger.info(" XXXXXXXXXX getProperty filePath : " + filePath
					+ " & property : " + property + " & exception "
					+ ex.getMessage());

			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw, true);
			// ex.printStackTrace(pw);
			pw.flush();
			sw.flush();

			value = property;
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e) {
				logger.info(" XXXXXXXXXXXXX getProperty Exception "
						+ e.getMessage());

				value = property;
			}
		}

		return value;
	}
	
	/**
	 * @author abdelrhim
	 */
	public static boolean hasPositiveValue(Long value)
	{
		if (value != null && value.intValue() > 0l)
			return true;
		
		return false;
	}
	
	/**
	 * @author abdelrhim
	 */
	public static boolean hasNegativeValue(Long value)
	{
		if (value == null || value.intValue() <= 0l)
			return true;
		
		return false;
	}
	
	/**
	 * @author abdelrhim
	 */
	public static boolean hasPositiveValue(Integer value)
	{
		if (value != null && value > 0l)
			return true;
		
		return false;
	}
	
	/**
	 * @author abdelrhim
	 */
	public static boolean hasNegativeValue(Integer value)
	{
		if (value == null || value <= 0l)
			return true;
		
		return false;
	}
}

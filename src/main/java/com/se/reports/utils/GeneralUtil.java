package com.se.reports.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeneralUtil {
	private static Logger logger = LoggerFactory.getLogger(GeneralUtil.class);
	private static final Properties CONFIG = new Properties();

	static{
		String reportsPropPath = Constants.OSPaths.LinuxReportsPropFile;
		if (OSDetector.isWindows()){
			reportsPropPath = Constants.OSPaths.reportsPropFile;
		}
		FileInputStream in = null;
		try{
			in = new FileInputStream(reportsPropPath);
			CONFIG.load(in);
		
		} catch (IOException e){
			throw new RuntimeException(new StringBuilder().append("Couldn't load properties from file: ").append(reportsPropPath).toString(), e);
		}
		finally{
			try{
				if (in != null)
					in.close();
			} catch (IOException e){
				logger.error("static block", e);
			}
		}
	}

	public static String constructStatmentPreparedStatment(StringBuilder sb, int paramCounts) {
		for(int i = 0; i < paramCounts; i++){
			if (i == 0){
				sb.append("?");
			}
			else{
				sb.append(", ?");
			}

		}

		sb.append(" )");

		return sb.toString();
	}



	public static String getProperty(String property) {
		return CONFIG.getProperty(property, "").trim();
	}



	public static String getQueryString(Map<String, String[]> urlParams) {
		StringBuilder queryString = new StringBuilder("?");
		for(Map.Entry entry : urlParams.entrySet()){
			queryString.append(new StringBuilder().append((String) entry.getKey()).append("=").toString());
			for(String v : (String[]) entry.getValue()){
				queryString.append(new StringBuilder().append(v).append(",").toString());
			}
			queryString.deleteCharAt(queryString.length() - 1);
			queryString.append("&");
		}
		queryString.deleteCharAt(queryString.length() - 1);

		return queryString.toString();
	}

	public static boolean isNumber(String input) {
		return input.matches("[0-9]*\\.?[0-9]*");
	}


	public static final boolean isBlank(Collection<?> collection) {
		if ((collection == null) || (collection.isEmpty())){
			return true;
		}
		return false;
	}

	public static String toString(Object rawString) {
		if (rawString == null){
			return null;
		}
		return rawString.toString();
	}



	public static String getORQueryString(String fieldName, List<String> values) {
		String query = new StringBuilder().append("  ").append(fieldName).append(" : ").toString();

		query = new StringBuilder().append(query).append(" ( ").toString();

		for(String item : values){
			query = new StringBuilder().append(query).append(" ").append(item).append(" OR ").toString();
		}

		query = query.substring(0, query.length() - 3);

		query = new StringBuilder().append(query).append(" ) ").toString();
		return query;
	}

	public static boolean hasValue(String s) {
		if (s != null && !s.trim().isEmpty()){
			return true;
		}
		return false;
	}
	public static boolean hasValue(Object object) {
		if (null != object){
			return true;
		}
		return false;
	}

	public static boolean isListEmpty(List<String> list) {
		if (list == null || list.isEmpty()){
			return true;
		}
		return false;
	}



	public static Long getLongValue(Object val) {
		if (hasValue(val)){
			return Long.valueOf(val.toString());
		}
		return 0l;
	}

	public static Integer getIntValue(Object val) {
		if (hasValue(val)){
			return Integer.valueOf(val.toString());
		}
		return 0;
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(Double.toString(value));
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
	
	//use to call price service by limit parts
	public static <T> List<Set<T>> split(Set<T> original, int count) {
		
	    // Create a list of sets to return.
	    ArrayList<Set<T>> result = new ArrayList<Set<T>>(count);

	    // Create an iterator for the original set.
	    Iterator<T> it = original.iterator();

	    // Calculate the required number of elements for each set.
	    int each = original.size() / count;

	    // Create each new set.
	    for (int i = 0; i < count; i++) {
	        HashSet<T> s = new HashSet<T>(original.size() / count + 1);
	        result.add(s);
	        for (int j = 0; j < each && it.hasNext(); j++) {
	            s.add(it.next());
	        }
	    }
	    return result;
	}


}

package com.se.reports.utils;

public class Constants {

	//Not used till now Configure by spring boot
	public static final String WINDOWS_PROPRETY_PATH = "D:/b001/app/configurations/se_reports_ws/";
	public static final String LINUX_PROPRETY_PATH = "/b001/app/configurations/se_reports_ws/";
	public static final String PROPRETY_FILE = "reports.properties";
	
	public static final String WEBSERVICE_BOM_URL = "${webservice.bom.url}";
	
	public static final String PRICE_SERVICE_AUTHENTICATION =  "${price.service.authentication}";
	public static final String PRICE_SERVICE_URL = "${price.service.url}";
	public static final String PRICE_SERVICE_LIMIT = "${price.service.limit}";
	public static final String REPORTS_LOGO_PATH = "${reports.logo.path:/b001/app/configurations/common_resources/SiliconExpertCN_large.png}";
	public static final String TRANSLATION_SERVICE_URL = "${webservice.search.url:http://localhost:8002/SearchBLService}";
	public static final String MAX_POOL_SIZE ="${max.pool.size:4}";
	public static final String CORE_POOL_SIZE ="${core.pool.size:4}";
	public static final String HOME_SERVICE_URL = "${webservice.home.url:http://localhost:8002/SE2HomeService}";
	

	public static class RequestType {
		public static final String GET = "GET";
		public static final String POST = "POST";
	}

	public static class OSPaths {	
		public static final String reportsPropFile = WINDOWS_PROPRETY_PATH + "\\" + PROPRETY_FILE;
		public static final String LinuxReportsPropFile = LINUX_PROPRETY_PATH + "/" + PROPRETY_FILE;
	}
	
	//start message	
	public static final String START_MESSAGE = "SE Reports API Application Started : ";
	
	//validation message 
	public static final String VALIDATE_USER_ID = "User ID Must valid ";
	public static final String PRICE_SERVICE_LIMIT_NOT_FOUND_AT_PROPERTY = "Couldn't Find PRICE_SERVICE_LIMIT at property file ";
	public static final String NO_COM_ID_RETURNED_FROM_BOM_SERVICE = "no returned com_id from bom service";
	
	//Report service identification
	public static final String PRICE_LEAD_SERVICE="Call this Service by Post Request";
	
	
	//ERROR Message
	public static final String NULL_FROM_BOM_SERVICE="NO Values returned from Bom service ";
	public static final String BOM_VALIDATION_ERROR_MESSAGE ="Error Message from BOM validation : ";
	
	//Price and Lead Report
	public static final String AVERGEPRICE="AVG Price";
	public static final String MINPRICE="Min Price";
	public static final String MINLEAD="Min Lead Time";
	public static final String MAXLEAD="Max Lead Time";
	public static final String LASTUPDATEDATE="Last Updated Date";	
	
	//Logger Constants
	public static final String BOM_LOGGER_TIME  = "time of execute getBomData(ReportsSeviceParamsDTO %s ) : %s MS";
	public static final String PRICE_SERVICE_LOGGER_TIME  ="time of execute getPriceLeadReportData(by : %s ComIDs ) : %s MS";
	public static final String PRICE_SERVICE_LOGGER_AUTHENTICATION = "PRICE_SERVICE_AUTHENTICATION : ";
	public static final String PRICE_SERVICE_LOGGER_AUTHENTICATION_SUCCESS_RESPONSE ="Authentication Succeeded";
	public static final String PRICE_SERVICE_AUTHENTICATION_FAILED ="Failed Authentication At Price Service";
	public static final String PRICE_SERVICE_THREAD_NAME = "PRICE SERVICE -";
	
	
	//Service Paths
	public static final String MAIN_REPORT_PATH  = "/report";
	public static final String PRICE_AND_LEAD_REPORT_PATH  ="/getpriceandleadreport";
	public static final String EXECUTER_NAME  ="asyncExecutor";
	
	
	public static final int LANGUAGE_ENGLISH = 1;
	public static final int LANGUAGE_DEUTSCH = 2;
	public static final int LANGUAGE_PORTUGUESE = 3;
	public static final int LANGUAGE_CHINESE = 4;
	public static final int LANGUAGE_TAIWANESE = 5;
}

package com.se.reports.exceptions;

import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;



@RestControllerAdvice
public class ReportSeviceExceptionHandling {
    @ExceptionHandler(MethodArgumentNotValidException.class)    
    public String handleException(MethodArgumentNotValidException e) {

		return "Implement Error Pages  : "
				+ "" + e.getLocalizedMessage();
    }
}

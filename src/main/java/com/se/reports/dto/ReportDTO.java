package com.se.reports.dto;

import java.util.ArrayList;

public class ReportDTO extends DTO {
	
	BomDTO bomDataDTO;
	
	private ArrayList<String> errorMessage =new ArrayList<String>();
	private StringBuilder statusMessage =new StringBuilder() ;
	
	private ServiceTimeDTO serviceTimeDTO =new ServiceTimeDTO();

	
	public BomDTO getBomDataDTO() {
		return bomDataDTO;
	}
	public void setBomDataDTO(BomDTO bomDataDTO) {
		this.bomDataDTO = bomDataDTO;
	}
	
	public ArrayList<String> getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(ArrayList<String> errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public StringBuilder getStatusMessage()
	{
		return statusMessage;
	}

	public void setStatusMessage(StringBuilder statusMessage)
	{
		this.statusMessage = statusMessage;
	}
	
	public ServiceTimeDTO getServiceTimeDTO()
	{
		return serviceTimeDTO;
	}

	public void setServiceTimeDTO(ServiceTimeDTO serviceTimeDTO)
	{
		this.serviceTimeDTO = serviceTimeDTO;
	}

}

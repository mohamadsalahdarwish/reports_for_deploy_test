package com.se.reports.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.se.reports.utils.Constants;

public class ReportsSeviceParamsDTO extends DTO {
	@NotNull(message = Constants.VALIDATE_USER_ID)
	@Min(1)
	private long userId;

	private int bomId;
	private int projectId;
	private int pageNumber;
	private int pageSize;
	// temp for BOM Service
	private int pageType = 1;
	
	private int language;

	private String locationHost;

	public ReportsSeviceParamsDTO() {
	}

	public ReportsSeviceParamsDTO(long userId, int bomId, int projectId, int pageNumber, int pageSize) {
		this.userId = userId;
		this.bomId = bomId;
		this.projectId = projectId;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
	}
	
	public ReportsSeviceParamsDTO(long userId, int bomId, int projectId, int pageNumber, int pageSize, int language) {
		this.userId = userId;
		this.bomId = bomId;
		this.projectId = projectId;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.language = language;
	}
	
	public ReportsSeviceParamsDTO(long userId, int bomId, int projectId, int pageNumber, int pageSize, int language, String locationHost) {
		this.userId = userId;
		this.bomId = bomId;
		this.projectId = projectId;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.language = language;
		this.setLocationHost(locationHost);
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getBomId() {
		return bomId;
	}

	public void setBomId(int bomId) {
		this.bomId = bomId;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageType() {
		return pageType;
	}

	public void setPageType(int pageType) {
		this.pageType = pageType;
	}
	
	public int getLanguage() {
		return language;
	}

	public void setLanguage(int language) {
		this.language = language;
	}

	@Override
	public String toString() {

		return super.toString();
	}

	public String getLocationHost() {
		return locationHost;
	}

	public void setLocationHost(String locationHost) {
		this.locationHost = locationHost;
	}

}
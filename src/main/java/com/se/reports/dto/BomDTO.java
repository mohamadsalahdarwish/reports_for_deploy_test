package com.se.reports.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BomDTO implements Serializable , Cloneable
{
	static final long serialVersionUID = 3611946474288743478L;
	
	 @JsonProperty("bomId")
	private long bomId;	
	 @JsonProperty("bomName")
	private String bomName;
	 @JsonProperty("errorMessage")
	private String errorMessage;
	 @JsonProperty("statusMessage")
	private StringBuilder statusMessage = new StringBuilder();

	 @JsonProperty("creationDate")
	private Date creationDate;
	 
	private List<BomMappedFeaturesDTO> bomMappedFeatureList = new ArrayList<BomMappedFeaturesDTO>();
	 
	private Date modificationDate;
	private String creationDateStr;
	private String modificationDateStr;
	private int bomKind; 	
	private int statusFlag; 
	
	private long projectId;	
//	private long parentProjectId;	
	private String projectName;	
	
	private String userName;
	private long userID;

	private String modifiedUser;
	private long modifiedUserId;



	private String siteName;
	private String companyName;
	
	boolean bomCached;
	
	private StatisticsDTO statisticsDTO = new StatisticsDTO();	
	
	private List<SingleRowStatusStatisticDTO>  rowStatusStatisticsList = new ArrayList<SingleRowStatusStatisticDTO>();
	

	//data
	private List<RowDTO> rowDTOList = new ArrayList<RowDTO>();
	private List<List<String>> allDataListOfList = new ArrayList<List<String>>(); 
	private List<List<String>> sampleData = new ArrayList<List<String>>(); 
	
	private UserNavigationDTO userNavigationDTO = new UserNavigationDTO();
	
	public BomDTO()
	{
		
	}
	
	@Override
	public Object clone()
	{
		Cloneable theClone = new BomDTO();
		// Initialize theClone.
		return theClone;
	}
	
	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}
	
	public StringBuilder getStatusMessage()
	{
		return statusMessage;
	}

	public void setStatusMessage(StringBuilder statusMessage)
	{
		this.statusMessage = statusMessage;
	}

	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}
	
	public Date getCreationDate()
	{
		return creationDate;
	}
	
	public void setModificationDate(Date modificationDate)
	{
		this.modificationDate = modificationDate;
	}
	
	public Date getModificationDate()
	{
		return modificationDate;
	}
	
	public String getCreationDateStr()
	{
		return creationDateStr;
	}

	public void setCreationDateStr(String creationDateStr)
	{
		this.creationDateStr = creationDateStr;
	}

	public String getModificationDateStr()
	{
		return modificationDateStr;
	}

	public void setModificationDateStr(String modificationDateStr)
	{
		this.modificationDateStr = modificationDateStr;
	}

	public void setBomId(long bomId)
	{
		this.bomId = bomId;
	}
	
	public long getBomId()
	{
		return bomId;
	}

	public void setBomName(String bomName)
	{
		this.bomName = bomName;
	}
	
	public String getBomName()
	{
		return bomName;
	}
	
//	public void setParentProjectId(long parentProjectId)
//	{
//		this.parentProjectId = parentProjectId;
//	}
//	
//	public long getParentProjectId()
//	{
//		return parentProjectId;
//	}
	
	public void setProjectName(String projectName)
	{
		this.projectName = projectName;
	}
	
	public String getProjectName()
	{
		return projectName;
	}
	
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	
	public String getUserName()
	{
		return userName;
	}
			
	public String getModifiedUser()
	{
		return modifiedUser;
	}

	public void setModifiedUser(String modifiedUser)
	{
		this.modifiedUser = modifiedUser;
	}
	
	public long getModifiedUserId() {
		return modifiedUserId;
	}

	public void setModifiedUserId(long modifiedUserId) {
		this.modifiedUserId = modifiedUserId;
	}

	public void setProjectId(long projectId)
	{
		this.projectId = projectId;
	}
	
	public long getProjectId()
	{
		return projectId;
	}
	
	
	public void setUserID(long userID)
	{
		this.userID = userID;
	}
	
	public long getUserID()
	{
		return userID;
	}
	
	public void setRowDTOList(List<RowDTO> rowDTOList)
	{
		this.rowDTOList = rowDTOList;
	}
	
	public List<RowDTO> getRowDTOList()
	{
		return rowDTOList;
	}

	public List<List<String>> getAllDataListOfList()
	{
		return allDataListOfList;
	}

	public void setAllDataListOfList(List<List<String>> allData)
	{
		this.allDataListOfList = allData;
	}
	
	public List<List<String>> getSampleData()
	{
		return sampleData;
	}

	public void setSampleData(List<List<String>> sampleData)
	{
		this.sampleData = sampleData;
	}
	
	public void setBomKind(int bomKind)
	{
		this.bomKind = bomKind;
	}
	
	public int getBomKind()
	{
		return bomKind;
	}


	public int getStatusFlag() {
		return statusFlag;
	}

	public void setStatusFlag(int statusFlag) {
		this.statusFlag = statusFlag;
	}

	public StatisticsDTO getStatisticsDTO() {
		return statisticsDTO;
	}

	public void setStatisticsDTO(StatisticsDTO statisticsDTO) {
		this.statisticsDTO = statisticsDTO;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public boolean isBomCached() {
		return bomCached;
	}

	public void setBomCached(boolean bomCached) {
		this.bomCached = bomCached;
	}

	public List<SingleRowStatusStatisticDTO> getRowStatusStatisticsList() {
		return rowStatusStatisticsList;
	}

	public void setRowStatusStatisticsList(List<SingleRowStatusStatisticDTO> rowStatusStatisticsList) {
		this.rowStatusStatisticsList = rowStatusStatisticsList;
	}

	public List<BomMappedFeaturesDTO> getBomMappedFeatureList() {
		return bomMappedFeatureList;
	}

	public void setBomMappedFeatureList(List<BomMappedFeaturesDTO> bomMappedFeatureList) {
		this.bomMappedFeatureList = bomMappedFeatureList;
	}


	@Override
	public String toString()
	{
		StringBuilder object = new StringBuilder();

		object.append(
						"--------Start BomDTO--------- \r\n" + 

						"bomId : "+bomId+" \r\n" + 
						"bomName : "+bomName+" \r\n" + 
						"errorMessage : "+errorMessage+" \r\n" + 
						"statusMessage : "+statusMessage+" \r\n" + 
						"creationDate : "+creationDate+" \r\n" + 
						"modificationDate : "+modificationDate+" \r\n" + 
						"creationDateStr : "+creationDateStr+" \r\n" + 
						"modificationDateStr : "+modificationDateStr+" \r\n" + 
						"bomKind : "+bomKind+" \r\n" + 
						"statusFlag : "+statusFlag+" \r\n" + 
						"projectId : "+projectId+" \r\n" + 
//						"parentProjectId : "+parentProjectId+" \r\n" + 
						"projectName : "+projectName+" \r\n" + 
						"userName : "+userName+" \r\n" + 
						"userID : "+userID+" \r\n" + 

						" \r\n" + 

						"statisticsDTO : "+statisticsDTO+" \r\n" + 										 
						"rowDTOList.size() : "+rowDTOList.size()+" \r\n" + 										
						"rowDTOList : "+rowDTOList+" \r\n" + 												
						"--------End BomDTO--------- \r\n" + 
						" \r\n" 
						);
		
		return object.toString();
	}

	public UserNavigationDTO getUserNavigationDTO() {
		return userNavigationDTO;
	}

	public void setUserNavigationDTO(UserNavigationDTO userNavigationDTO) {
		this.userNavigationDTO = userNavigationDTO;
	}
	
}

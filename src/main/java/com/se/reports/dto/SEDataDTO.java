package com.se.reports.dto;

import java.io.Serializable;



public class SEDataDTO implements Serializable// , Cloneable
{
	public SEDataDTO()
	{
		
	}
	
	static final long serialVersionUID = 2658946783697933478L;

	private long comId;

	private String partnumber;
	
	private String manName;

	private long manId;

	private String desc;

	private String pdfUrl;

	//Product line name & Id
	private String plName;
	private String smallPartImage;

	private long plId;

	private String taxName;	

	private String rohs ;

	private String rohsVersion;

	private String chinaRohs;

	private String lifeCycle ;

	private String lifecycleDate;

	private String yeol;									

	private String yeolComment;							

	private long crossCount;

	private long replacementCount;

	//SE Grade
	private int starCount;

	private String reach;

	//Inventory 
	private long invCountOriginal;

	private long invCountOptions;
	
	private long invHistoryFlag;
	
	private double avgPrice;

	private double minPrice;

	private int pcnCount;
		
	//Obsolescence PCNs
	private int pcnCountObs;
		
	private int partOptionWithInvCount;

	private long totalPartWeight;

	private int isComponentArchived; 					// 1 = component, 2 = archive

	private String confidenceLevel;

	private int levenshteinDistance;
	
	private String lastCheckDate;
							
	public long getComId() {
		return comId;
	}

	public void setComId(long comId) {
		this.comId = comId;
	}

	public String getPartnumber() {
		return partnumber;
	}

	public void setPartnumber(String partnumber) {
		this.partnumber = partnumber;
	}

	public String getManName() {
		return manName;
	}

	public void setManName(String manName) {
		this.manName = manName;
	}

	public long getManId() {
		return manId;
	}

	public void setManId(long manId) {
		this.manId = manId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getPdfUrl() {
		return pdfUrl;
	}

	public void setPdfUrl(String pdfUrl) {
		this.pdfUrl = pdfUrl;
	}

	public String getPlName() {
		return plName;
	}

	public void setPlName(String plName) {
		this.plName = plName;
	}
	
	public String getSmallPartImage() {
		return smallPartImage;
	}

	public void setSmallPartImage(String smallPartImage) {
		this.smallPartImage = smallPartImage;
	}

	public long getPlId() {
		return plId;
	}

	public void setPlId(long plId) {
		this.plId = plId;
	}

	public String getTaxName() {
		return taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

	public String getRohs() {
		return rohs;
	}

	public void setRohs(String rohs) {
		this.rohs = rohs;
	}

	public String getRohsVersion() {
		return rohsVersion;
	}

	public void setRohsVersion(String rohsVersion) {
		this.rohsVersion = rohsVersion;
	}

	public String getChinaRohs() {
		return chinaRohs;
	}

	public void setChinaRohs(String chinaRohs) {
		this.chinaRohs = chinaRohs;
	}

	public String getLifeCycle() {
		return lifeCycle;
	}

	public void setLifeCycle(String lifeCycle) {
		this.lifeCycle = lifeCycle;
	}
	
	public String getLifecycleDate() {
		return lifecycleDate;
	}

	public void setLifecycleDate(String lifecycleDate) {
		this.lifecycleDate = lifecycleDate;
	}

	public String getYeol() {
		return yeol;
	}

	public void setYeol(String yeol) {
		this.yeol = yeol;
	}

	public String getYeolComment() {
		return yeolComment;
	}

	public void setYeolComment(String yeolComment) {
		this.yeolComment = yeolComment;
	}

	public long getCrossCount() {
		return crossCount;
	}

	public void setCrossCount(long crossCount) {
		this.crossCount = crossCount;
	}

	public long getReplacementCount() {
		return replacementCount;
	}

	public void setReplacementCount(long replacementCount) {
		this.replacementCount = replacementCount;
	}

	public int getStarCount() {
		return starCount;
	}

	public void setStarCount(int starCount) {
		this.starCount = starCount;
	}

	public String getReach() {
		return reach;
	}

	public void setReach(String reach) {
		this.reach = reach;
	}

	public long getInvCountOriginal() {
		return invCountOriginal;
	}

	public void setInvCountOriginal(long invCountOriginal) {
		this.invCountOriginal = invCountOriginal;
	}

	public long getInvCountOptions() {
		return invCountOptions;
	}

	public void setInvCountOptions(long invCountOptions) {
		this.invCountOptions = invCountOptions;
	}

	public long getInvHistoryFlag() {
		return invHistoryFlag;
	}

	public void setInvHistoryFlag(long invHistoryFlag) {
		this.invHistoryFlag = invHistoryFlag;
	}

	public double getAvgPrice() {
		return avgPrice;
	}

	public void setAvgPrice(double avgPrice) {
		this.avgPrice = avgPrice;
	}

	public double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(double minPrice) {
		this.minPrice = minPrice;
	}

	public int getPcnCount() {
		return pcnCount;
	}

	public void setPcnCount(int pcnCount) {
		this.pcnCount = pcnCount;
	}

	public int getPcnCountObs() {
		return pcnCountObs;
	}

	public void setPcnCountObs(int pcnCountObs) {
		this.pcnCountObs = pcnCountObs;
	}
		
	public int getPartOptionWithInvCount() {
		return partOptionWithInvCount;
	}

	public void setPartOptionWithInvCount(int partOptionWithInvCount) {
		this.partOptionWithInvCount = partOptionWithInvCount;
	}

	public long getTotalPartWeight() {
		return totalPartWeight;
	}

	public void setTotalPartWeight(long totalPartWeight) {
		this.totalPartWeight = totalPartWeight;
	}

	public int getIsComponentArchived() {
		return isComponentArchived;
	}

	public void setIsComponentArchived(int isComponentArchived) {
		this.isComponentArchived = isComponentArchived;
	}

	public String getConfidenceLevel() {
		return confidenceLevel;
	}

	public void setConfidenceLevel(String confidenceLevel) {
		this.confidenceLevel = confidenceLevel;
	}

	public int getLevenshteinDistance() {
		return levenshteinDistance;
	}

	public void setLevenshteinDistance(int levenshteinDistance) {
		this.levenshteinDistance = levenshteinDistance;
	}

	public String getLastCheckDate() {
		return lastCheckDate;
	}

	public void setLastCheckDate(String lastCheckDate) {
		this.lastCheckDate = lastCheckDate;
	}
							

	
	
}

package com.se.reports.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserNavigationDTO implements Serializable
{
	private static final long serialVersionUID = -4729785654991077568L;
		
	private long bomId;
	private long projectId;
	private long userId;	
	private int userType;			// 1 : Registered, 		2 : Part details paid,		4: Bom Paid
	
	//Filter   OR 
	private String rohsFilter;   
	private String lifeCycleFilter;      
	
//	match  , notMatch  , ignored 
	private String matchStatusFilter;
	
//	matchExact   ,matchAuto   ,matchReplaced   , notMatchSimilar ,notMatchNone , ignored
	private String matchTypeFilter;  
	
	private Integer rowStatusFilterId; 
	
//	Medium, Low ,	High , Unknown			
	private List<String> lifecycleRiskFilter = new ArrayList<String>();								

	private List<String> environmentalRiskFilter = new ArrayList<String>();
	
	private List<String> inventoryRiskFilter = new ArrayList<String>();
	
	private List<String> multiSourceRiskFilter = new ArrayList<String>();
			
	private List<String> overallRiskFilter = new ArrayList<String>();

	//hasPCN , noPcn
	private String pcnFilter;  
	
	//Pagination 
	private int pageNumber;
	private int pageSize;
	private int numberOfPages;	
   
	//go to this row , in case has value , will ignore filtration  \r\n sorting criterias
	private int rowNumber;    
	
	//language for the API use it in download 
	private int language;			// 1 : English,		2 : Deutsch,	3 : Portuguese,		4  : Chinese,	5 : Taiwanese
	
	
	/**
	 * rownum
	 * uploaded mpn
	 * uploaded man
	 * uploaded mapped features ?????		cpn , price , quantity , other features
	 * 
	 * se mpn
	 * se man
	 * 
	 * rohs
	 * lifecycle
	 * 
	 * crossess
	 * replacesment
	 * 
	 * product line
	 * taxonamy tax path
	 *  
	 * eol
	 * starCount
	 * 
	 * invCountOriginal
	 * pcnCount
	 * 
	 * lifecycleRisk
	 * environmentalRisk
	 * inventoryRisk
	 * multiSourceRisk
	 * overallRisk

	 * 
	 * */
	private String sortField;
	private int sortType;			//1 : ASC,   2 : DESC

	private int pageType;			//1 : browse, 2 : match, 3 : riskOverall, 4 : riskLifecycle, 5 : riskMultiSource, 6 : riskEnvironmental, 7 : riskInventory	
	
//	private DataFilteredDTO dataFilteredDTO = new DataFilteredDTO();

	
	public long getUserId()
	{
		return userId;
	}
	public String getMatchStatusFilter() {
		return matchStatusFilter;
	}
	public void setMatchStatusFilter(String matchStatusFilter) {
		this.matchStatusFilter = matchStatusFilter;
	}
	public String getMatchTypeFilter() {
		return matchTypeFilter;
	}
	public void setMatchTypeFilter(String matchTypeFilter) {
		this.matchTypeFilter = matchTypeFilter;
	}
	public List<String> getLifecycleRiskFilter() {
		return lifecycleRiskFilter;
	}
	public void setLifecycleRiskFilter(List<String> lifecycleRiskFilter) {
		this.lifecycleRiskFilter = lifecycleRiskFilter;
	}
	public List<String> getEnvironmentalRiskFilter() {
		return environmentalRiskFilter;
	}
	public void setEnvironmentalRiskFilter(List<String> environmentalRiskFilter) {
		this.environmentalRiskFilter = environmentalRiskFilter;
	}
	public List<String> getInventoryRiskFilter() {
		return inventoryRiskFilter;
	}
	public void setInventoryRiskFilter(List<String> inventoryRiskFilter) {
		this.inventoryRiskFilter = inventoryRiskFilter;
	}
	public List<String> getMultiSourceRiskFilter() {
		return multiSourceRiskFilter;
	}
	public void setMultiSourceRiskFilter(List<String> multiSourceRiskFilter) {
		this.multiSourceRiskFilter = multiSourceRiskFilter;
	}
	public List<String> getOverallRiskFilter() {
		return overallRiskFilter;
	}
	public void setOverallRiskFilter(List<String> overallRiskFilter) {
		this.overallRiskFilter = overallRiskFilter;
	}
	public String getPcnFilter() {
		return pcnFilter;
	}
	public void setPcnFilter(String pcnFilter) {
		this.pcnFilter = pcnFilter;
	}
	public void setUserId(long userId)
	{
		this.userId = userId;
	}
	public int getUserType()
	{
		return userType;
	}
	public void setUserType(int userType)
	{
		this.userType = userType;
	}
	public int getPageType()
	{
		return pageType;
	}
	public void setPageType(int pageType)
	{
		this.pageType = pageType;
	}
	public long getBomId()
	{
		return bomId;
	}
	public void setBomId(long bomId)
	{
		this.bomId = bomId;
	}
	public long getProjectId() {
		return projectId;
	}
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}
	public String getRohsFilter()
	{
		return rohsFilter;
	}
	public void setRohsFilter(String rohsFilter)
	{
		this.rohsFilter = rohsFilter;
	}
	public String getLifeCycleFilter()
	{
		return lifeCycleFilter;
	}
	public void setLifeCycleFilter(String lifeCycleFilter)
	{
		this.lifeCycleFilter = lifeCycleFilter;
	}	
	public int getPageNumber()
	{
		return pageNumber;
	}
	public void setPageNumber(int pageNumber)
	{
		this.pageNumber = pageNumber;
	}
	public int getPageSize()
	{
		return pageSize;
	}
	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}
	public int getNumberOfPages()
	{
		return numberOfPages;
	}
	public void setNumberOfPages(int numberOfPages)
	{
		this.numberOfPages = numberOfPages;
	}
	public int getRowNumber() {
		return rowNumber;
	}
	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}
	public int getLanguage()
	{
		return language;
	}
	public void setLanguage(int language)
	{
		this.language = language;
	}
	public String getSortField()
	{
		return sortField;
	}
	public void setSortField(String sortField)
	{
		this.sortField = sortField;
	}
	
	public int getSortType()
	{
		return sortType;
	}
	public void setSortType(int sortType)
	{
		this.sortType = sortType;
	}
	
	
//	public DataFilteredDTO getDataFilteredDTO() {
//		return dataFilteredDTO;
//	}
//	public void setDataFilteredDTO(DataFilteredDTO dataFilteredDTO) {
//		this.dataFilteredDTO = dataFilteredDTO;
//	}
	
	
	
	public Integer getRowStatusFilterId() {
		return rowStatusFilterId;
	}
	public void setRowStatusFilterId(Integer rowStatusFilterId) {
		this.rowStatusFilterId = rowStatusFilterId;
	}
	public String toString()
	{
		StringBuilder object = new StringBuilder();

		object.append(
						"-----------------  \r\n" + 

						"bomId : "+bomId+"  \r\n" + 
						
						"userId : "+userId+"  \r\n" + 
						"userType : "+userType+"  \r\n" + 
						"pageType : "+pageType+"  \r\n" + 
						
						"rohsFilter : "+rohsFilter+"  \r\n" + 
						"lifeCycleFilter : "+lifeCycleFilter+"  \r\n" + 
						"matchStatusFilter : "+matchStatusFilter+"  \r\n" + 
						
						"pageNumber : "+pageNumber+"  \r\n" + 
						"pageSize : "+pageSize+"  \r\n" + 
						"numberOfPages : "+numberOfPages+"  \r\n" + 
						
						"language : "+language+"  \r\n" + 
//						"dataFilteredDTO : "+dataFilteredDTO+"  \r\n" + 
						
						"-----------------  \r\n" + 
						"  \r\n" 
					);
		
		return object.toString();
	}
}

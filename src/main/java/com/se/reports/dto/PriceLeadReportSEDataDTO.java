package com.se.reports.dto;

public class PriceLeadReportSEDataDTO extends ReportsSEDataDTO {
	

	//price Fields 
	private String avergePrice;
	private String minPrice;
	private String minLead;
	private String maxLead;
	private String lastUpdateDate;
	
	
	public String getAvergePrice() {
		return avergePrice;
	}
	public void setAvergePrice(String avergePrice) {
		this.avergePrice = avergePrice;
	}
	public String getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}
	public String getMinLead() {
		return minLead;
	}
	public void setMinLead(String minLead) {
		this.minLead = minLead;
	}
	public String getMaxLead() {
		return maxLead;
	}
	public void setMaxLead(String maxLead) {
		this.maxLead = maxLead;
	}
	public String getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	

	

}

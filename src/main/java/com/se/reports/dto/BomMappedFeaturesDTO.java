package com.se.reports.dto;


import java.io.Serializable;

public class BomMappedFeaturesDTO implements Serializable // ,Cloneable
{
	static final long serialVersionUID = 1658169583697933478L;	
	
	public BomMappedFeaturesDTO()
	{
		
	}
	
//    public Object clone() {
//        Cloneable theClone = new BomMappedFeaturesDTO();
//        // Initialize theClone.
//        return theClone;
//    }
    
	public BomMappedFeaturesDTO(String columnName)
	{
		super();
		this.columnName = columnName;
	}
	
    private long bomMappedFeatureId;

    private long bomId;

    private String columnName;
    
    private int columnNumber;
        
	public long getBomMappedFeatureId()
	{
		return bomMappedFeatureId;
	}

	public void setBomMappedFeatureId(long bomMappedFeatureId)
	{
		this.bomMappedFeatureId = bomMappedFeatureId;
	}

	public long getBomId()
	{
		return bomId;
	}

	public void setBomId(long bomId)
	{
		this.bomId = bomId;
	}

	public String getColumnName()
	{
		return columnName;
	}

	public void setColumnName(String columnName)
	{
		this.columnName = columnName;
	}

	public int getColumnNumber()
	{
		return columnNumber;
	}

	public void setColumnNumber(int columnNumber)
	{
		this.columnNumber = columnNumber;
	}
	
	public String toString()
	{
		StringBuilder object = new StringBuilder();

		object.append(
						"----------------- &" + 

						"bomMappedFeatureId : "+bomMappedFeatureId+" &" + 
						
						"bomId : "+bomId+" &" + 
						"columnName : "+columnName+" &" + 
						"columnNumber : "+columnNumber+" &" + 
																		
						"----------------- &" + 
						" &" 
					);
		
		return object.toString();
	}

}


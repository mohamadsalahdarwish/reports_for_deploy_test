package com.se.reports.dto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class DTO {
	
//	public abstract String toHTMLTable();
	
	public DTO() {
		// TODO Auto-generated constructor stub
	}
	
	public String toJson() {
//		this.toHTMLTable();
		Gson gson = new GsonBuilder().disableHtmlEscaping().serializeSpecialFloatingPointValues().create();
		return gson.toJson(this);
	}
//	
//	@Override
//	public String toString() {
//		return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
//	}
	
}

package com.se.reports.dto;


import java.util.ArrayList;
import java.util.List;

public class ValidationDTO extends DTO{
boolean isValid;
List<String> errorMessages = new ArrayList<String>();
public boolean isValid() {
	return isValid;
}
public void setValid(boolean isValid) {
	this.isValid = isValid;
}
public List<String> getErrorMessages() {
	return errorMessages;
}
public void setErrorMessages(List<String> errorMessages) {
	this.errorMessages = errorMessages;
}

}

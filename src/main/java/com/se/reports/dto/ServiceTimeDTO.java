package com.se.reports.dto;

public class ServiceTimeDTO extends DTO {

	//use to calculate reports time
	private long allReportServiceTime;
	private long bomServiceTime;
	
	//to get price service Time
	private long priceServiceTime;
	private long authenticationTime;
	

	public long getAllReportServiceTime() {
		return allReportServiceTime;
	}

	public void setAllReportServiceTime(long allReportServiceTime) {
		this.allReportServiceTime = allReportServiceTime;
	}

	public long getBomServiceTime() {
		return bomServiceTime;
	}

	public void setBomServiceTime(long bomServiceTime) {
		this.bomServiceTime = bomServiceTime;
	}
	
	public long getPriceServiceTime() {
		return priceServiceTime;
	}

	public void setPriceServiceTime(long priceServiceTime) {
		this.priceServiceTime = priceServiceTime;
	}
	
	public long getAuthenticationTime() {
		return authenticationTime;
	}

	public void setAuthenticationTime(long authenticationTime) {
		this.authenticationTime = authenticationTime;
	}
	
}

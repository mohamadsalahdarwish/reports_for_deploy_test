package com.se.reports.dto;

import java.io.Serializable;

/**
 * @author hany_shaker
 *
 */
public class StatisticsDTO implements Serializable
{	
	private static final long serialVersionUID = -4502598486155664770L;
	
	private int totalParts;

	//all exact parts
	private int matchCount;
	private int matchRatioInt;	
	private float matchRatioFloat;
	
	//match exact parts not edited
	private int matchExactCount;
	private int matchExactRatioInt;	
	private float matchExactRatioFloat;
	
	//auto correction parts
	private int matchAutoCount;
	private int matchAutoRatioInt;	
	private float matchAutoRatioFloat;
	
	//replaced corrected parts
	private int matchReplacedCount;
	private int matchReplacedRatioInt;	
	private float matchReplacedRatioFloat;
	
	
	//No match + Similar
	private int notMatchCount;	
	private int notMatchRatioInt;	
	private float notMatchRatioFloat;
	
	//Similar only
	private int notMatchSimilarCount;	
	private int notMatchSimilarRatioInt;	
	private float notMatchSimilarRatioFloat;
	
	//No match only
	private int notMatchNoneCount;	
	private int notMatchNoneRatioInt;	
	private float notMatchNoneRatioFloat;

	//ignored	
	private int ignoredCount;	
	private int ignoredRatioInt;	
	private float ignoredRatioFloat;
	
	private int totalSuppliers;
	private int numberOfIdentifiedSuppliers;
	private int numberOfUnidentifiedSuppliers;
		
	public int getTotalParts() {
		return totalParts;
	}
	public void setTotalParts(int totalParts) {
		this.totalParts = totalParts;
	}
	public int getMatchCount() {
		return matchCount;
	}
	public void setMatchCount(int matchCount) {
		this.matchCount = matchCount;
	}
	public int getMatchRatioInt() {
		return matchRatioInt;
	}
	public void setMatchRatioInt(int matchRatioInt) {
		this.matchRatioInt = matchRatioInt;
	}
	public float getMatchRatioFloat() {
		return matchRatioFloat;
	}
	public void setMatchRatioFloat(float matchRatioFloat) {
		this.matchRatioFloat = matchRatioFloat;
	}
	public int getMatchExactCount() {
		return matchExactCount;
	}
	public void setMatchExactCount(int matchExactCount) {
		this.matchExactCount = matchExactCount;
	}
	public int getMatchExactRatioInt() {
		return matchExactRatioInt;
	}
	public void setMatchExactRatioInt(int matchExactRatioInt) {
		this.matchExactRatioInt = matchExactRatioInt;
	}
	public float getMatchExactRatioFloat() {
		return matchExactRatioFloat;
	}
	public void setMatchExactRatioFloat(float matchExactRatioFloat) {
		this.matchExactRatioFloat = matchExactRatioFloat;
	}
	public int getMatchAutoCount() {
		return matchAutoCount;
	}
	public void setMatchAutoCount(int matchAutoCount) {
		this.matchAutoCount = matchAutoCount;
	}
	public int getMatchAutoRatioInt() {
		return matchAutoRatioInt;
	}
	public void setMatchAutoRatioInt(int matchAutoRatioInt) {
		this.matchAutoRatioInt = matchAutoRatioInt;
	}
	public float getMatchAutoRatioFloat() {
		return matchAutoRatioFloat;
	}
	public void setMatchAutoRatioFloat(float matchAutoRatioFloat) {
		this.matchAutoRatioFloat = matchAutoRatioFloat;
	}
	public int getMatchReplacedCount() {
		return matchReplacedCount;
	}
	public void setMatchReplacedCount(int matchReplacedCount) {
		this.matchReplacedCount = matchReplacedCount;
	}
	public int getMatchReplacedRatioInt() {
		return matchReplacedRatioInt;
	}
	public void setMatchReplacedRatioInt(int matchReplacedRatioInt) {
		this.matchReplacedRatioInt = matchReplacedRatioInt;
	}
	public float getMatchReplacedRatioFloat() {
		return matchReplacedRatioFloat;
	}
	public void setMatchReplacedRatioFloat(float matchReplacedRatioFloat) {
		this.matchReplacedRatioFloat = matchReplacedRatioFloat;
	}
	public int getNotMatchCount() {
		return notMatchCount;
	}
	public void setNotMatchCount(int notMatchCount) {
		this.notMatchCount = notMatchCount;
	}
	public int getNotMatchRatioInt() {
		return notMatchRatioInt;
	}
	public void setNotMatchRatioInt(int notMatchRatioInt) {
		this.notMatchRatioInt = notMatchRatioInt;
	}
	public float getNotMatchRatioFloat() {
		return notMatchRatioFloat;
	}
	public void setNotMatchRatioFloat(float notMatchRatioFloat) {
		this.notMatchRatioFloat = notMatchRatioFloat;
	}
	public int getNotMatchSimilarCount() {
		return notMatchSimilarCount;
	}
	public void setNotMatchSimilarCount(int notMatchSimilarCount) {
		this.notMatchSimilarCount = notMatchSimilarCount;
	}
	public int getNotMatchSimilarRatioInt() {
		return notMatchSimilarRatioInt;
	}
	public void setNotMatchSimilarRatioInt(int notMatchSimilarRatioInt) {
		this.notMatchSimilarRatioInt = notMatchSimilarRatioInt;
	}
	public float getNotMatchSimilarRatioFloat() {
		return notMatchSimilarRatioFloat;
	}
	public void setNotMatchSimilarRatioFloat(float notMatchSimilarRatioFloat) {
		this.notMatchSimilarRatioFloat = notMatchSimilarRatioFloat;
	}
	public int getNotMatchNoneCount() {
		return notMatchNoneCount;
	}
	public void setNotMatchNoneCount(int notMatchNoneCount) {
		this.notMatchNoneCount = notMatchNoneCount;
	}
	public int getNotMatchNoneRatioInt() {
		return notMatchNoneRatioInt;
	}
	public void setNotMatchNoneRatioInt(int notMatchNoneRatioInt) {
		this.notMatchNoneRatioInt = notMatchNoneRatioInt;
	}
	public float getNotMatchNoneRatioFloat() {
		return notMatchNoneRatioFloat;
	}
	public void setNotMatchNoneRatioFloat(float notMatchNoneRatioFloat) {
		this.notMatchNoneRatioFloat = notMatchNoneRatioFloat;
	}
	public int getIgnoredCount() {
		return ignoredCount;
	}
	public void setIgnoredCount(int ignoredCount) {
		this.ignoredCount = ignoredCount;
	}
	public int getIgnoredRatioInt() {
		return ignoredRatioInt;
	}
	public void setIgnoredRatioInt(int ignoredRatioInt) {
		this.ignoredRatioInt = ignoredRatioInt;
	}
	public float getIgnoredRatioFloat() {
		return ignoredRatioFloat;
	}
	public void setIgnoredRatioFloat(float ignoredRatioFloat) {
		this.ignoredRatioFloat = ignoredRatioFloat;
	}
	public int getTotalSuppliers() {
		return totalSuppliers;
	}
	public void setTotalSuppliers(int totalSuppliers) {
		this.totalSuppliers = totalSuppliers;
	}
	public int getNumberOfIdentifiedSuppliers() {
		return numberOfIdentifiedSuppliers;
	}
	public void setNumberOfIdentifiedSuppliers(int numberOfIdentifiedSuppliers) {
		this.numberOfIdentifiedSuppliers = numberOfIdentifiedSuppliers;
	}
	public int getNumberOfUnidentifiedSuppliers() {
		return numberOfUnidentifiedSuppliers;
	}
	public void setNumberOfUnidentifiedSuppliers(int numberOfUnidentifiedSuppliers) {
		this.numberOfUnidentifiedSuppliers = numberOfUnidentifiedSuppliers;
	}

	
	
	
	
	
	
	@Override
	public String toString()
	{
		StringBuilder object = new StringBuilder();

		object.append(

						"totalParts : "+totalParts+" & " + 
						
						"matchCount : "+matchCount+" & " + 
//						"matchRatioInt : "+matchRatioInt+" & " + 
//						"matchRatioFloat : "+matchRatioFloat+" & " + 
						
						"matchExactCount : "+matchExactCount+" & " + 
//						"matchExactRatioInt : "+matchExactRatioInt+" & " + 
//						"matchExactRatioFloat : "+matchExactRatioFloat+" & " + 
						
						"matchAutoCount : "+matchAutoCount+" & " + 
//						"matchAutoRatioInt : "+matchAutoRatioInt+" & " + 
//						"matchAutoRatioFloat : "+matchAutoRatioFloat+" & " + 
						
						"matchReplacedCount : "+matchReplacedCount+" & " + 
//						"matchReplacedRatioInt : "+matchReplacedRatioInt+" & " + 
//						"matchReplacedRatioFloat : "+matchReplacedRatioFloat+" & " + 
						
						"notMatchCount : "+notMatchCount+" & " + 
//						"notMatchRatioInt : "+notMatchRatioInt+" & " + 
//						"notMatchRatioFloat : "+notMatchRatioFloat+" & " + 
						
						"notMatchSimilarCount : "+notMatchSimilarCount+" & " + 
//						"notMatchSimilarRatioInt : "+notMatchSimilarRatioInt+" & " + 
//						"notMatchSimilarRatioFloat : "+notMatchSimilarRatioFloat+" & " + 
						
						"notMatchNoneCount : "+notMatchNoneCount+" & " + 
//						"notMatchNoneRatioInt : "+notMatchNoneRatioInt+" & " + 
//						"notMatchNoneRatioFloat : "+notMatchNoneRatioFloat+" & " + 
						
						"ignoredCount : "+ignoredCount+//" & " + 
//						"ignoredRatioInt : "+ignoredRatioInt+" & " + 
//						"ignoredRatioFloat : "+ignoredRatioFloat+" & " + 
						
//						"totalSuppliers : "+totalSuppliers+" & " + 
//						"numberOfIdentifiedSuppliers : "+numberOfIdentifiedSuppliers+" & " + 
//						"numberOfUnidentifiedSuppliers : "+numberOfUnidentifiedSuppliers+" & " + 

						" " 
						);
		
		return object.toString();
	}
	
	
	
	
	
	
}

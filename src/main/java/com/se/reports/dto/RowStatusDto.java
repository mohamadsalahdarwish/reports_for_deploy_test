package com.se.reports.dto;

import java.io.Serializable;

public class RowStatusDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9185582786964687874L;

	private Integer statusId;

	private String statusMsg;

	public RowStatusDto() {
		this.statusId = 1;
		this.statusMsg = "";
	}

	public RowStatusDto(Integer statusId, String statusMsg) {
		super();
		this.statusId = statusId;
		this.statusMsg = statusMsg;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

}

package com.se.reports.dto;

import java.io.Serializable;

public class SingleRowStatusStatisticDTO implements Serializable {

	private static final long serialVersionUID = 824953987174998244L;

	private Integer statusId;

	private String value;

	private Integer count;

	private Integer ratioInt;

	private Float ratioFloat;

	private Integer order;
	
	public SingleRowStatusStatisticDTO() {
		statusId = 0;
		value = "";
		count = 0;
		ratioInt = 0;
		ratioFloat = 0.0f;
		order = 0;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}


	public Integer getRatioInt() {
		return ratioInt;
	}

	public void setRatioInt(Integer ratioInt) {
		this.ratioInt = ratioInt;
	}

	public Float getRatioFloat() {
		return ratioFloat;
	}

	public void setRatioFloat(Float ratioFloat) {
		this.ratioFloat = ratioFloat;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	
}

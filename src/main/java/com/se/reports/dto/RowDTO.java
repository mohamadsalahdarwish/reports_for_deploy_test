package com.se.reports.dto;

import java.io.Serializable;

/**
 * @author hany_shaker
 *
 */
public class RowDTO implements Serializable , Cloneable
{	
	static final long serialVersionUID = 2658946783697933478L;

	public RowDTO()
	{
		
	}
		
	@Override
	public Object clone()
	{
		Cloneable theClone = new RowDTO();

		return theClone;
	}
	
	private long bomResultId;
	
	private int rowNum;
	
	private int resultType;

	private int oldResultType;

	private String matchType;
	
	private long comId;
	
	private long originalComId;

	private long oldComId;

	private String uploadedMpn;
	
	private String nanMpn;
	
	private String seNanMpn;
	
	private String matchedMpn;
	
	private String seMatchedMpn;

	private String uploadedSupplier;
	
	private String matchedSupplier;

	private String seMatchedSupplier;

	private long manId;

	private long seManId;

	private long oldManId;

	//attributes related only to save similar to avoid saving new se data mpn & man 
//	private String similarNanMpn;
//	
//	private String similarMatchedMpn;
//		
//	private String similarMatchedSupplier;	
//
//	private long similarManId;

	
	private String uploadedCpn;
	
	private String uploadedDescription;
	
	private String uploadedPrice;
	
	private String uploadedQuantity;
	
	private int editFlag;			//0 : NOT_EDITED,    1 : EDITED ,     2 : LOOKUP
		
	private int ignoredFlag;
	
	private int statusFlag;
	
	private String validationMessage;
		
	private int similarOptionsCount;
		
	private boolean hideSeData;
	
	private String partialNanMpn; // First 3 char
	
	private String col1;
	
	private String col2;
	
	private String col3;
	
	private String col4;
	
	private String col5;
	
	private String col6;
	
	private String col7;
	
	private String col8;
	
	private String col9;
	
	private String col10;
	
	private String col11;
	
	private String col12;
	
	private String col13;
	
	private String col14;
	
	private String col15;
	
	private RowStatusDto  rowStatusDto = new RowStatusDto();
	
	private SEDataDTO seDataDTO = new SEDataDTO();
	private ReportsSEDataDTO reportsSEDataDTO;		
	public void setRowNum(int rowNum)
	{
		this.rowNum = rowNum;
	}
	
	public int getRowNum()
	{
		return rowNum;
	}
	
	public void setUploadedMpn(String uploadedMpn)
	{
		this.uploadedMpn = uploadedMpn;
	}
	
	public String getUploadedMpn()
	{
		return uploadedMpn;
	}
	
	public String getNanMpn()
	{
		return nanMpn;
	}
	
	public void setNanMpn(String nanMpn)
	{
		this.nanMpn = nanMpn;
	}
		
	public String getSeNanMpn() {
		return seNanMpn;
	}

	public void setSeNanMpn(String seNanMpn) {
		this.seNanMpn = seNanMpn;
	}

	public String getMatchedMpn()
	{
		return matchedMpn;
	}
	
	public void setMatchedMpn(String matchedMpn)
	{
		this.matchedMpn = matchedMpn;
	}
	
	public String getSeMatchedMpn() {
		return seMatchedMpn;
	}

	public void setSeMatchedMpn(String seMatchedMpn) {
		this.seMatchedMpn = seMatchedMpn;
	}

	public void setUploadedSupplier(String uploadedSupplier)
	{
		this.uploadedSupplier = uploadedSupplier;
	}
	
	public String getUploadedSupplier()
	{
		return uploadedSupplier;
	}
	
	public String getMatchedSupplier()
	{
		return matchedSupplier;
	}
	
	public void setMatchedSupplier(String matchedSupplier)
	{
		this.matchedSupplier = matchedSupplier;
	}
		
	public String getSeMatchedSupplier() {
		return seMatchedSupplier;
	}

	public void setSeMatchedSupplier(String seMatchedSupplier) {
		this.seMatchedSupplier = seMatchedSupplier;
	}

	public void setComId(long comId)
	{
		this.comId = comId;
	}
	
	public long getComId()
	{
		return comId;
	}
		
	public long getOriginalComId() {
		return originalComId;
	}

	public void setOriginalComId(long originalComId) {
		this.originalComId = originalComId;
	}

	public void setResultType(int resultType)
	{
		this.resultType = resultType;
	}
	
	public int getResultType()
	{
		return resultType;
	}
		
	public int getOldResultType() {
		return oldResultType;
	}

	public void setOldResultType(int oldResultType) {
		this.oldResultType = oldResultType;
	}

	public long getOldComId() {
		return oldComId;
	}

	public void setOldComId(long oldComId) {
		this.oldComId = oldComId;
	}

	public void setMatchTypeValue(String matchType)
	{
		this.matchType=matchType;
	}
	
	public String getMatchType() {
		return matchType;
	}

	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}

	public SEDataDTO getSeDataDTO()
	{
		return seDataDTO;
	}

	public void setSeDataDTO(SEDataDTO seDataDTO)
	{
		this.seDataDTO = seDataDTO;
	}

	public void setManId(long manId)
	{
		this.manId = manId;
	}
	
	public long getManId()
	{
		return manId;
	}
		
	public long getSeManId() {
		return seManId;
	}

	public void setSeManId(long seManId) {
		this.seManId = seManId;
	}

	public long getOldManId() {
		return oldManId;
	}

	public void setOldManId(long oldManId) {
		this.oldManId = oldManId;
	}

	public void setUploadedCpn(String uploadedCpn)
	{
		this.uploadedCpn = uploadedCpn;
	}
	
	public String getUploadedCpn()
	{
		return uploadedCpn;
	}
	
	public String getUploadedPrice()
	{
		return uploadedPrice;
	}
	
	
	public void setUploadedPrice(String uploadedPrice)
	{
		this.uploadedPrice = uploadedPrice;
	}
	
	
	public String getUploadedQuantity()
	{
		return uploadedQuantity;
	}
	
	
	public void setUploadedQuantity(String uploadedQuantity)
	{
		this.uploadedQuantity = uploadedQuantity;
	}
	
	
	public void setBomResultId(long bomResultId)
	{
		this.bomResultId = bomResultId;
	}
	
	public long getBomResultId()
	{
		return bomResultId;
	}
	
	
	public void setUploadedDescription(String uploadedDescription)
	{
		this.uploadedDescription = uploadedDescription;
	}
	
	public String getUploadedDescription()
	{
		return uploadedDescription;
	}
	
	public String getPartialNanMpn()
	{
		return partialNanMpn;
	}
	
	public void setPartialNanMpn(String partialNanMpn)
	{
		this.partialNanMpn = partialNanMpn;
	}
	
	public String getCol1()
	{
		return col1;
	}
	
	
	public void setCol1(String col1)
	{
		this.col1 = col1;
	}
	
	
	public String getCol2()
	{
		return col2;
	}
	
	
	public void setCol2(String col2)
	{
		this.col2 = col2;
	}
	
	
	public String getCol3()
	{
		return col3;
	}
	
	
	public void setCol3(String col3)
	{
		this.col3 = col3;
	}
	
	
	public String getCol4()
	{
		return col4;
	}
	
	
	public void setCol4(String col4)
	{
		this.col4 = col4;
	}
	
	
	public String getCol5()
	{
		return col5;
	}
	
	
	public void setCol5(String col5)
	{
		this.col5 = col5;
	}
	
	
	public String getCol6()
	{
		return col6;
	}
	
	
	public void setCol6(String col6)
	{
		this.col6 = col6;
	}
	
	
	public String getCol7()
	{
		return col7;
	}
	
	
	public void setCol7(String col7)
	{
		this.col7 = col7;
	}
	
	
	public String getCol8()
	{
		return col8;
	}
	
	
	public void setCol8(String col8)
	{
		this.col8 = col8;
	}
	
	
	public String getCol9()
	{
		return col9;
	}
	
	
	public void setCol9(String col9)
	{
		this.col9 = col9;
	}
	
	
	public String getCol10()
	{
		return col10;
	}
	
	
	public void setCol10(String col10)
	{
		this.col10 = col10;
	}
	
	
	public String getCol11()
	{
		return col11;
	}
	
	
	public void setCol11(String col11)
	{
		this.col11 = col11;
	}
	
	
	public String getCol12()
	{
		return col12;
	}
	
	
	public void setCol12(String col12)
	{
		this.col12 = col12;
	}
	
	
	public String getCol13()
	{
		return col13;
	}
	
	
	public void setCol13(String col13)
	{
		this.col13 = col13;
	}
	
	
	public String getCol14()
	{
		return col14;
	}
	
	
	public void setCol14(String col14)
	{
		this.col14 = col14;
	}
	
	
	public String getCol15()
	{
		return col15;
	}
	
	
	public void setCol15(String col15)
	{
		this.col15 = col15;
	}
	
	public int getEditFlag()
	{
		return editFlag;
	}
	
	public void setEditFlag(int editFlag)
	{
		this.editFlag = editFlag;
	}
	
	public int getIgnoredFlag()
	{
		return ignoredFlag;
	}
	
	public void setIgnoredFlag(int ignoredFlag)
	{
		this.ignoredFlag = ignoredFlag;
	}
		
	public int getStatusFlag()
	{
		return statusFlag;
	}
	
	public void setStatusFlag(int statusFlag)
	{
		this.statusFlag = statusFlag;
	}
		
	public String getValidationMessage()
	{
		return validationMessage;
	}
	
	public void setValidationMessage(String validationMessage)
	{
		this.validationMessage = validationMessage;
	}
		
	public int getSimilarOptionsCount()
	{
		return similarOptionsCount;
	}
	
	public void setSimilarOptionsCount(int similarOptionsCount)
	{
		this.similarOptionsCount = similarOptionsCount;
	}
	
	public boolean isHideSeData()
	{
		return hideSeData;
	}

	public void setHideSeData(boolean hideSeData)
	{
		this.hideSeData = hideSeData;
	}

	public RowStatusDto getRowStatusDto() {
		return rowStatusDto;
	}

	public void setRowStatusDto(RowStatusDto rowStatusDto) {
			this.rowStatusDto = rowStatusDto;	
	}
	
	public ReportsSEDataDTO getReportsSEDataDTO() {
		return reportsSEDataDTO;
	}

	public void setReportsSEDataDTO(ReportsSEDataDTO reportsSEDataDTO) {
			this.reportsSEDataDTO = reportsSEDataDTO;	
	}
	

	@Override
	public String toString()
	{
		StringBuilder object = new StringBuilder();

		object.append(
						"----------------- &" + 

						"bomResultId : "+bomResultId+" &" + 
						"rowNum : "+rowNum+" &" + 
						"resultType : "+resultType+" &" + 
						"comId : "+comId+" &" + 
						"uploadedMpn : "+uploadedMpn+" &" + 

						"nanMpn : "+nanMpn+" &" + 
						"matchedMpn : "+matchedMpn+" &" + 
						"uploadedSupplier : "+uploadedSupplier+" &" + 
						"matchedSupplier : "+matchedSupplier+" &" + 
						"manId : "+manId+" &" + 

						"uploadedCpn : "+uploadedCpn+" &" + 
						"uploadedDescription : "+uploadedDescription+" &" + 
						"uploadedPrice : "+uploadedPrice+" &" + 
						"uploadedQuantity : "+uploadedQuantity+" &" + 
						"editFlag : "+editFlag+" &" + 

						"ignoredFlag : "+ignoredFlag+" &" + 
						"statusFlag : "+statusFlag+" &" + 
						"validationMessage : "+validationMessage+" &" + 
						"similarOptionsCount : "+similarOptionsCount+" &" + 
						"partialNanMpn : "+partialNanMpn+" &" +
						
						"col1 : "+col1+" &" + 
						"col2 : "+col2+" &" + 
						"col3 : "+col3+" &" + 
						"col4 : "+col4+" &" + 
						"col5 : "+col5+" &" + 

						"col6 : "+col6+" &" + 
						"col7 : "+col7+" &" + 
						"col8 : "+col8+" &" + 
						"col9 : "+col9+" &" + 
						"col10 : "+col10+" &" + 

						"col11 : "+col11+" &" + 
						"col12 : "+col12+" &" + 
						"col13 : "+col13+" &" + 
						"col14 : "+col14+" &" + 
						"col15 : "+col15+" &" +						
						"----------------- &" + 
						" &" 
					);
		
		return object.toString();
	}
}

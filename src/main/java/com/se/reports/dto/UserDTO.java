package com.se.reports.dto;

public class UserDTO {
	private float id;
	private String email;
	private float status;
	private float passwordSet;
	private float loginCount;
	private float created;
	private float lastAccess;
	private float addPasswordSent;
	private float expired;
	private float accessTokenUsed;
	private float uRoleId;
	private float companyId;
	private float siteId;
	private String phone;
	private String fax = null;
	private String firstName;
	private String lastName;
	private float autoLogin;
	private float disabledFlag;
	private float termsofUse;
	private float termsOfUseDate;
	private float se2UserType;
	private float changePWFlag;
	private float loggedInUserID;
	private float msgFlag;
	private String companyName;
	private String expireDate;
	private float maxParts;
	private float usedParts;
	private boolean passwordExpired;
	private String lastPWChangeDate = null;

	// Getter Methods

	public float getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public float getStatus() {
		return status;
	}

	public float getPasswordSet() {
		return passwordSet;
	}

	public float getLoginCount() {
		return loginCount;
	}

	public float getCreated() {
		return created;
	}

	public float getLastAccess() {
		return lastAccess;
	}

	public float getAddPasswordSent() {
		return addPasswordSent;
	}

	public float getExpired() {
		return expired;
	}

	public float getAccessTokenUsed() {
		return accessTokenUsed;
	}

	public float getURoleId() {
		return uRoleId;
	}

	public float getCompanyId() {
		return companyId;
	}

	public float getSiteId() {
		return siteId;
	}

	public String getPhone() {
		return phone;
	}

	public String getFax() {
		return fax;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public float getAutoLogin() {
		return autoLogin;
	}

	public float getDisabledFlag() {
		return disabledFlag;
	}

	public float getTermsofUse() {
		return termsofUse;
	}

	public float getTermsOfUseDate() {
		return termsOfUseDate;
	}

	public float getSe2UserType() {
		return se2UserType;
	}

	public float getChangePWFlag() {
		return changePWFlag;
	}

	public float getLoggedInUserID() {
		return loggedInUserID;
	}

	public float getMsgFlag() {
		return msgFlag;
	}

	public String getCompanyName() {
		return companyName;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public float getMaxParts() {
		return maxParts;
	}

	public float getUsedParts() {
		return usedParts;
	}

	public boolean getPasswordExpired() {
		return passwordExpired;
	}

	public String getLastPWChangeDate() {
		return lastPWChangeDate;
	}

	// Setter Methods

	public void setId(float id) {
		this.id = id;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setStatus(float status) {
		this.status = status;
	}

	public void setPasswordSet(float passwordSet) {
		this.passwordSet = passwordSet;
	}

	public void setLoginCount(float loginCount) {
		this.loginCount = loginCount;
	}

	public void setCreated(float created) {
		this.created = created;
	}

	public void setLastAccess(float lastAccess) {
		this.lastAccess = lastAccess;
	}

	public void setAddPasswordSent(float addPasswordSent) {
		this.addPasswordSent = addPasswordSent;
	}

	public void setExpired(float expired) {
		this.expired = expired;
	}

	public void setAccessTokenUsed(float accessTokenUsed) {
		this.accessTokenUsed = accessTokenUsed;
	}

	public void setURoleId(float uRoleId) {
		this.uRoleId = uRoleId;
	}

	public void setCompanyId(float companyId) {
		this.companyId = companyId;
	}

	public void setSiteId(float siteId) {
		this.siteId = siteId;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setAutoLogin(float autoLogin) {
		this.autoLogin = autoLogin;
	}

	public void setDisabledFlag(float disabledFlag) {
		this.disabledFlag = disabledFlag;
	}

	public void setTermsofUse(float termsofUse) {
		this.termsofUse = termsofUse;
	}

	public void setTermsOfUseDate(float termsOfUseDate) {
		this.termsOfUseDate = termsOfUseDate;
	}

	public void setSe2UserType(float se2UserType) {
		this.se2UserType = se2UserType;
	}

	public void setChangePWFlag(float changePWFlag) {
		this.changePWFlag = changePWFlag;
	}

	public void setLoggedInUserID(float loggedInUserID) {
		this.loggedInUserID = loggedInUserID;
	}

	public void setMsgFlag(float msgFlag) {
		this.msgFlag = msgFlag;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public void setMaxParts(float maxParts) {
		this.maxParts = maxParts;
	}

	public void setUsedParts(float usedParts) {
		this.usedParts = usedParts;
	}

	public void setPasswordExpired(boolean passwordExpired) {
		this.passwordExpired = passwordExpired;
	}

	public void setLastPWChangeDate(String lastPWChangeDate) {
		this.lastPWChangeDate = lastPWChangeDate;
	}
}
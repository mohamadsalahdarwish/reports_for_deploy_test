package com.se.reports.dto;

import java.io.Serializable;

public class LeadAndPriceReportDTO implements Serializable {

	private static final long serialVersionUID = -5939599057698930678L;

	public LeadAndPriceReportDTO() {

	}

	private long bomId;

	private int row;
	private String uploadedCpn;
	private String partNumber;
	private String supplier;
	private String matchType;

	//price Fields 
	private String avergePrice;
	private String minPrice;
	private String minLead;
	private String maxLead;
	private String lastUpdateDate;
	
	//private bomdto bomdto;
	//private pricedto pricedto;
	
	public long getBomId() {
		return bomId;
	}
	public void setBomId(long bomId) {
		this.bomId = bomId;
	}
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public String getUploadedCpn() {
		return uploadedCpn;
	}
	public void setUploadedCpn(String uploadedCpn) {
		this.uploadedCpn = uploadedCpn;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getMatchType() {
		return matchType;
	}
	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}
	public String getAvergePrice() {
		return avergePrice;
	}
	public void setAvergePrice(String avergePrice) {
		this.avergePrice = avergePrice;
	}
	public String getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}
	public String getMinLead() {
		return minLead;
	}
	public void setMinLead(String minLead) {
		this.minLead = minLead;
	}
	public String getMaxLead() {
		return maxLead;
	}
	public void setMaxLead(String maxLead) {
		this.maxLead = maxLead;
	}
	public String getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	

	
	
}
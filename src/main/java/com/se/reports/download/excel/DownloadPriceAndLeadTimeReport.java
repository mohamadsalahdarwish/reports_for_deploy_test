package com.se.reports.download.excel;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.se.reports.apiclients.home.HomeClient;
import com.se.reports.apiclients.translation.TranslationClient;
import com.se.reports.bussiness.impl.LeadAndPriceReportServiceImpl;
import com.se.reports.download.util.CaseInsensitiveMap;
import com.se.reports.download.util.ReportsUtil;
import com.se.reports.dto.BomDTO;
import com.se.reports.dto.BomMappedFeaturesDTO;
import com.se.reports.dto.PriceLeadReportSEDataDTO;
import com.se.reports.dto.ReportDTO;
import com.se.reports.dto.ReportsSeviceParamsDTO;
import com.se.reports.dto.RowDTO;
import com.se.reports.dto.SEDataDTO;
import com.se.reports.dto.UserDTO;
import com.se.reports.utils.Constants;
import com.streaming.excel.silicon.excel.builder.ExcelStreaming;
import com.streaming.excel.silicon.excel.builder.template.CellStyleTemplate;
import com.streaming.excel.silicon.excel.builder.template.CellTemplate;
import com.streaming.excel.silicon.excel.builder.template.RowTemplate;
import com.streaming.excel.silicon.excel.builder.template.SheetAttribute;
import com.streaming.excel.silicon.excel.builder.template.SheetTemplate;

@Component
public class DownloadPriceAndLeadTimeReport extends DownloadExcelReport {
	
	final static Logger logger = Logger.getLogger(LeadAndPriceReportServiceImpl.class);
	
	@Value(Constants.REPORTS_LOGO_PATH)
	private String reportsLogoPath;
	
	@Autowired
	private TranslationClient translationClient;
	
	@Autowired
	private HomeClient homeClient;
	
	@Override
	public ByteArrayInputStream drawReport(ExcelStreaming excelStreaming, ReportDTO reportDTO, ReportsSeviceParamsDTO reportsSeviceParamsDTO) {
		
		ByteArrayInputStream result = null;
		
		try {			
		
			CaseInsensitiveMap translatedMap = (CaseInsensitiveMap) translationClient.callTranslationService(reportsSeviceParamsDTO.getLanguage());
			UserDTO userDTO = homeClient.callHomeUserService(reportsSeviceParamsDTO.getUserId());
			
			BomDTO bomDTO = reportDTO.getBomDataDTO();
			String reportName = "Price & Lead Time Report";	
			
			SheetTemplate sheetTemplate = null;
			
			if(bomDTO.getProjectName() != null && !bomDTO.getProjectName().isEmpty()){
				sheetTemplate = excelStreaming.createSheet(reportName, reportName, 
						new SheetAttribute(ReportsUtil.getTranslatedWord(translatedMap, "User Name"), userDTO.getFirstName()),
						new SheetAttribute(ReportsUtil.getTranslatedWord(translatedMap, "Date"), String.format("%1$tB %1$td, %1$tY", new Date())),
						new SheetAttribute("Project", ReportsUtil.getTranslatedWord(translatedMap, bomDTO.getProjectName())),
						new SheetAttribute(ReportsUtil.getTranslatedWord(translatedMap, "BOM Name"), bomDTO.getBomName()));
			}else{
				sheetTemplate = excelStreaming.createSheet(reportName, reportName, 
						new SheetAttribute(ReportsUtil.getTranslatedWord(translatedMap, "User Name"), userDTO.getFirstName()),
						new SheetAttribute(ReportsUtil.getTranslatedWord(translatedMap, "Date"), String.format("%1$tB %1$td, %1$tY", new Date())),
						new SheetAttribute(ReportsUtil.getTranslatedWord(translatedMap, "BOM Name"), bomDTO.getBomName()));
			}
			
						
			drawHeader(excelStreaming, sheetTemplate, reportDTO, translatedMap);			
			drawData(excelStreaming, sheetTemplate, reportDTO, translatedMap, reportsSeviceParamsDTO.getLocationHost());
			
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream(2048);
			excelStreaming.createWorkbook(true, this.reportsLogoPath, outputStream);
			
			System.out.println("test" + reportsLogoPath);
			
			result = new ByteArrayInputStream(outputStream.toByteArray());
		
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error("ERROR", e);
		}
		return result;		
	}

	@Override
	public SheetTemplate drawHeader(ExcelStreaming excelStreaming, SheetTemplate sheetTemplate, ReportDTO reportDTO, CaseInsensitiveMap translatedMap) {
		
		try{
			// getBoms Data
			final BomDTO bomDTO = reportDTO.getBomDataDTO();			
			List<BomMappedFeaturesDTO> bomMappedFeaturesDTOs = bomDTO.getBomMappedFeatureList();
			
			// getBasicHeader
			final List<String> header = new ArrayList<>();
			
			// Customer Data
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "Row"));
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "Uploaded Part"));
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "Uploaded Manufacturer"));			
			
			if(bomMappedFeaturesDTOs != null && !bomMappedFeaturesDTOs.isEmpty()){
				for(BomMappedFeaturesDTO bomMappedFeaturesDTO : bomMappedFeaturesDTOs){
					if(!"Uploaded Part".equalsIgnoreCase(bomMappedFeaturesDTO.getColumnName()) &&
					   !"Uploaded Manufacturer".equalsIgnoreCase(bomMappedFeaturesDTO.getColumnName()) &&
					   !"CPN".equalsIgnoreCase(bomMappedFeaturesDTO.getColumnName()) &&
					   !"PRICE".equalsIgnoreCase(bomMappedFeaturesDTO.getColumnName()) &&
					   !"QUANTITY".equalsIgnoreCase(bomMappedFeaturesDTO.getColumnName())){
						header.add(ReportsUtil.getTranslatedWord(translatedMap, bomMappedFeaturesDTO.getColumnName()));
					}					
				}
			}
			
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "CPN"));
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "PRICE"));
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "QUANTITY"));
			
			// SE Data
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "SiliconExpert MPN"));
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "SiliconExpert Supplier"));
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "Match Status"));
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "Product Line"));
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "Description"));
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "Datasheet"));
			
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "Average Price"));
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "Minimum Price"));
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "Minimum Lead Time (Week(s))"));
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "Maximum Lead Time (Week(s))"));
			header.add(ReportsUtil.getTranslatedWord(translatedMap, "Last Update Date"));	
			
	
			List<RowTemplate> rowTemplates = new ArrayList<RowTemplate>();
			sheetTemplate.setDataList(rowTemplates);
			// / Create Sheet
			RowTemplate newHeader = excelStreaming.createRow();
			rowTemplates.add(newHeader);
			newHeader.setCells(excelStreaming.createCells(header));
			newHeader.setHeader(true);
			
		
		}catch(Throwable e){
			e.printStackTrace();
			logger.error("ERROR", e);
		}
		
		return sheetTemplate;
	}

	@Override
	public SheetTemplate drawData(ExcelStreaming excelStreaming, SheetTemplate sheetTemplate, ReportDTO reportDTO, CaseInsensitiveMap translatedMap, String locationHost) {
		
		try{
			final BomDTO bomDTO = reportDTO.getBomDataDTO();
			
			// Create Data
			final List<RowDTO> rowDTOs = bomDTO.getRowDTOList();
			CellStyleTemplate contentDataPartStyle = excelStreaming.createStyle();
			contentDataPartStyle.setWrapped(true);
						
			for (int i = 0; i < rowDTOs.size(); i++) {
				
				RowTemplate row = excelStreaming.createRow();
				row.setCellStyleTemplate(contentDataPartStyle);
				List<CellTemplate> cell = new ArrayList<CellTemplate>();
				row.setCells(cell);
			
				exportCustomerData(excelStreaming, sheetTemplate, row, rowDTOs.get(i), bomDTO.getBomMappedFeatureList());				
				exportSEData(excelStreaming, row, rowDTOs.get(i), translatedMap, locationHost);
			}

		}catch(Throwable e){
			e.printStackTrace();
			logger.error("ERROR", e);
		}
		
		return sheetTemplate;
	}	
	
	private void exportCustomerData(ExcelStreaming excelStreaming, SheetTemplate sheetTemplate, RowTemplate rowTemplate, RowDTO rowDTO, List<BomMappedFeaturesDTO> bomMappedFeaturesDTOs) {
		
		try {		
		
			RowTemplate row = null;
			if (rowTemplate == null) {
				row = excelStreaming.createRow();
				row.setCells(new ArrayList<CellTemplate>());
			} else {
				row = rowTemplate;
			}
	
			sheetTemplate.getDataList().add(row);
	
			row.getCells().add(excelStreaming.createCell(rowDTO.getRowNum() + ""));
			row.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(rowDTO.getUploadedMpn())));
			row.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(rowDTO.getUploadedSupplier())));
			
			if(bomMappedFeaturesDTOs != null && !bomMappedFeaturesDTOs.isEmpty()){
				for(BomMappedFeaturesDTO bomMappedFeaturesDTO : bomMappedFeaturesDTOs){
					if(!"Uploaded Part".equalsIgnoreCase(bomMappedFeaturesDTO.getColumnName()) &&
					   !"Uploaded Manufacturer".equalsIgnoreCase(bomMappedFeaturesDTO.getColumnName()) &&
					   !"CPN".equalsIgnoreCase(bomMappedFeaturesDTO.getColumnName()) &&
					   !"PRICE".equalsIgnoreCase(bomMappedFeaturesDTO.getColumnName()) &&
					   !"QUANTITY".equalsIgnoreCase(bomMappedFeaturesDTO.getColumnName())){
						String columnData = ReportsUtil.getCustomColumnData(rowDTO, bomMappedFeaturesDTO.getColumnNumber());
						row.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(columnData)));
					}
				}
			}
			
			row.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(rowDTO.getUploadedCpn())));
			row.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(rowDTO.getUploadedPrice())));
			row.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(rowDTO.getUploadedQuantity())));
			
			
			
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error("ERROR", e);
		}
	}	
	
	private void exportSEData(ExcelStreaming excelStreaming, RowTemplate rowTemplate, RowDTO rowDTO, CaseInsensitiveMap translatedMap, String locationHost) {
		
		try {			
			SEDataDTO seDataDTO = rowDTO.getSeDataDTO();			
			
			if(seDataDTO != null){
				rowTemplate.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(seDataDTO.getPartnumber())));
				rowTemplate.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(seDataDTO.getManName())));
				rowTemplate.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(rowDTO.getMatchType()))); // Match Status
				rowTemplate.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(seDataDTO.getPlName())));
				rowTemplate.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(seDataDTO.getDesc())));
				
				if(seDataDTO.getPdfUrl() != null){
					String newPdfUrl = (seDataDTO.getPdfUrl().toLowerCase().contains("download.siliconexpert") ? ReportsUtil.updateUrlByHost(seDataDTO.getPdfUrl(), locationHost) : seDataDTO.getPdfUrl());
					rowTemplate.getCells().add(excelStreaming.createCell(newPdfUrl, true, ReportsUtil.getTranslatedWord(translatedMap, "Link to Datasheet")));
				}else{
					rowTemplate.getCells().add(excelStreaming.createCell(""));
				}
			}
			
			PriceLeadReportSEDataDTO priceLeadReportSEDataDTO = (PriceLeadReportSEDataDTO) rowDTO.getReportsSEDataDTO();
			
			if(priceLeadReportSEDataDTO != null){
				rowTemplate.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(priceLeadReportSEDataDTO.getAvergePrice())));
				rowTemplate.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(priceLeadReportSEDataDTO.getMinPrice())));
				rowTemplate.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(priceLeadReportSEDataDTO.getMinLead())));
				rowTemplate.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(priceLeadReportSEDataDTO.getMaxLead())));
				rowTemplate.getCells().add(excelStreaming.createCell(ReportsUtil.checkNotNull(priceLeadReportSEDataDTO.getLastUpdateDate())));
			}
			
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error("ERROR", e);
		}
	}	
}

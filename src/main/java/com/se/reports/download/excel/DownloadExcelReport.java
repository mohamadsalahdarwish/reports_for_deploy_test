package com.se.reports.download.excel;

import java.io.ByteArrayInputStream;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.se.reports.download.util.CaseInsensitiveMap;
import com.se.reports.dto.ReportDTO;
import com.se.reports.dto.ReportsSeviceParamsDTO;
import com.streaming.excel.silicon.excel.builder.ExcelStreaming;
import com.streaming.excel.silicon.excel.builder.template.SheetTemplate;

@Component
public abstract class DownloadExcelReport {	
		
	public abstract ByteArrayInputStream drawReport(ExcelStreaming excelStreaming, ReportDTO reportDTO, ReportsSeviceParamsDTO reportsSeviceParamsDTO);
	
	public abstract SheetTemplate drawHeader(ExcelStreaming excelStreaming, SheetTemplate sheet, ReportDTO reportDTO, CaseInsensitiveMap translatedMap);
	
	public abstract SheetTemplate drawData(ExcelStreaming excelStreaming, SheetTemplate sheet, ReportDTO reportDTO, CaseInsensitiveMap translatedMap, String locationHost);
	
	
}

package com.se.reports.download.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.apache.log4j.Logger;

import com.se.reports.dto.RowDTO;



public class ReportsUtil {
	
	final static Logger logger = Logger.getLogger(ReportsUtil.class);
	
	public static String replaceSpecialCharacterWithUnderscore(String str) {
		return str.replaceAll("[^a-zA-Z0-9]+", "_");
	}
	
	public static String checkNotNull(String value) {
		if(value != null)
			return value;
		else
			return "";
	}
	
	public static String getCustomColumnData(RowDTO rowDTO, int columnNumber)
	{		
		String columnData = "";
		
		switch (columnNumber)
		{
			case 1:
				columnData = rowDTO.getCol1();
				break;
			case 2:
				columnData = rowDTO.getCol2();
				break;
			case 3:
				columnData = rowDTO.getCol3();
				break;
			case 4:
				columnData = rowDTO.getCol4();
				break;
			case 5:
				columnData = rowDTO.getCol5();
				break;
			case 6:
				columnData = rowDTO.getCol6();
				break;
			case 7:
				columnData = rowDTO.getCol7();
				break;
			case 8:
				columnData = rowDTO.getCol8();
				break;
			case 9:
				columnData = rowDTO.getCol9();
				break;
			case 10:
				columnData = rowDTO.getCol10();
				break;
			case 11:
				columnData = rowDTO.getCol11();
				break;
			case 12:
				columnData = rowDTO.getCol12();
				break;
			case 13:
				columnData = rowDTO.getCol13();
				break;
			case 14:
				columnData = rowDTO.getCol14();
				break;
			case 15:
				columnData = rowDTO.getCol15();
				break;
		}
				
		return columnData;
	}
	
	public static String getTranslatedWord(CaseInsensitiveMap translatedMap, String key)
	{
		String word="";
		try {
		if(translatedMap.get(key)!=null && !translatedMap.get(key).equals(""))
		{
			word = translatedMap.get(key);
			
		}else {
			word = key;
			//logger.info("cannot find translation key for "+key);
		}
		}
		catch(Throwable e)
		{
			e.printStackTrace();
			logger.error("ERROR", e);
		}
		
		word = (word != null ? word.toUpperCase() : "");
		return word;
	}	
	
	public static String updateUrlByHost(String url, String host)
			throws MalformedURLException {
		
		if(url == null || url.isEmpty() || host == null || host.isEmpty()){
			return url;
		}
		URL originalURL = new URL(url);

		String newHost = host.substring(host.lastIndexOf(".") + 1);
		newHost = originalURL.getHost().substring(0, originalURL.getHost().lastIndexOf(".") + 1) + newHost;
		URL newURL = new URL("https", newHost, originalURL.getFile());
		return (newURL != null ? newURL.toExternalForm() : "");
	}
}

package com.se.reports.services;

import java.util.ArrayList;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.se.reports.bussiness.LeadAndPriceReportService;
import com.se.reports.dto.ReportDTO;
import com.se.reports.dto.ReportsSeviceParamsDTO;
import com.se.reports.utils.Constants;

@RestController
@RequestMapping(Constants.MAIN_REPORT_PATH)
public class ReportSevice {
	
	final static Logger logger = Logger.getLogger(ReportSevice.class);
	
	@Autowired
	LeadAndPriceReportService leadAndPriceReportService;
	
	@PostMapping(value =Constants.PRICE_AND_LEAD_REPORT_PATH , consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getPriceAndLeadReport(@Valid @RequestBody ReportsSeviceParamsDTO reportsSeviceParamsDTO) {
		logger.info("Calling getPriceAndLeadReport("+reportsSeviceParamsDTO.toJson()+") at : "+System.currentTimeMillis());
		ReportDTO reportDTO = leadAndPriceReportService.getReportData(reportsSeviceParamsDTO);
		if(!reportDTO.getErrorMessage().isEmpty()) {
			return new ResponseEntity<ArrayList<String>>(reportDTO.getErrorMessage(), HttpStatus.OK);	
		}else {
			return new ResponseEntity<ReportDTO>(reportDTO, HttpStatus.OK);	
		}		
	}	
	
	@GetMapping(value = "/downloadpriceandleadreport/{bomID}/{isProject}/{language}/{userID}/{userType}/{locationHost}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<?> downloadPriceAndLeadReport(
			@PathVariable("bomID") int bomID,
			@PathVariable("isProject") boolean isProject,
			@PathVariable("language") int language,
			@PathVariable("userID") long userID,
			@PathVariable("userType") int userType,
			@PathVariable("locationHost") String locationHost) {
		
        ReportsSeviceParamsDTO reportsSeviceParamsDTO = new ReportsSeviceParamsDTO(userID, bomID, 0, 1, 1000000, language, locationHost);
        
        long l = System.currentTimeMillis();
        logger.info("Before calling downloadReportData, " + userID + "," + bomID + "," + 0 + "," + 1 + "," + 1000000 + "," + language);
		ResponseEntity<?> response = leadAndPriceReportService.downloadReportData(reportsSeviceParamsDTO);		
		logger.info("After calling downloadReportData, elapsed time = " + (System.currentTimeMillis() - l) + " ms");
		return response;		
			
	}
	
}

package com.se.reports.apiclients;

import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

public class Client {

	@Bean
	public RestTemplate rest() {
	return new RestTemplate();
	}
}

package com.se.reports.apiclients.price;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.se.reports.apiclients.Client;
import com.se.reports.dto.PriceLeadReportSEDataDTO;
import com.se.reports.dto.ReportDTO;
import com.se.reports.dto.RowDTO;
import com.se.reports.utils.Constants;
import com.se.reports.utils.MyPartition;

@Service
public class PriceClient extends Client {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	PriceAsyncClientService priceAsyncClientService;

	@Value(Constants.PRICE_SERVICE_AUTHENTICATION)
	private String PRICE_SERVICE_AUTHENTICATION;

	@Value(Constants.PRICE_SERVICE_LIMIT)
	private int PRICE_SERVICE_LIMIT;

	final static Logger logger = Logger.getLogger(PriceClient.class);

	//use to Authentication by first request 
	private void getAuthentication(ReportDTO retReportDTO) {
		long authenticationTime = System.currentTimeMillis();
		try {
		CookieManager manager = new CookieManager();
		manager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
		CookieHandler.setDefault(manager);			
		ResponseEntity<String> s = restTemplate.getForEntity(PRICE_SERVICE_AUTHENTICATION, String.class);
		logger.info(Constants.PRICE_SERVICE_LOGGER_AUTHENTICATION + s.getBody());
		if(!s.getBody().contains(Constants.PRICE_SERVICE_LOGGER_AUTHENTICATION_SUCCESS_RESPONSE)) {			
			retReportDTO.getErrorMessage().add(Constants.PRICE_SERVICE_AUTHENTICATION_FAILED);
		}
		}catch (Throwable e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			retReportDTO.getErrorMessage().add(e.getMessage());
		}	
		authenticationTime =System.currentTimeMillis()-authenticationTime;
		retReportDTO.getServiceTimeDTO().setAuthenticationTime((retReportDTO.getServiceTimeDTO().getAuthenticationTime()+authenticationTime));
	}
	
	public ReportDTO getPriceLeadReportData(ReportDTO retReportDTO) {
		long priceServiceCallingTime = System.currentTimeMillis();
		try {
			getAuthentication(retReportDTO);
			if(!retReportDTO.getErrorMessage().isEmpty()) {
				return retReportDTO;
			}
			getPriceData(retReportDTO);
			logger.info(String.format(Constants.PRICE_SERVICE_LOGGER_TIME,
					retReportDTO.getBomDataDTO().getRowDTOList().size(),
					(System.currentTimeMillis() - priceServiceCallingTime)));
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error(e);
		}
		retReportDTO.getServiceTimeDTO().setPriceServiceTime((System.currentTimeMillis() - priceServiceCallingTime));
		return retReportDTO;
	}

	private Map<Long, List<RowDTO>> getComIDsMap(List<RowDTO> rowDTOList) {
		Map<Long, List<RowDTO>> comIDsDTOMap = new HashMap<Long, List<RowDTO>>();
		if (rowDTOList != null && rowDTOList.size() > 0) {
			for (RowDTO rowDTO : rowDTOList) {
				List<RowDTO> dtos = comIDsDTOMap.get(rowDTO.getComId());
				if (dtos != null) {
					dtos.add(rowDTO);
				} else {
					ArrayList<RowDTO> arrayList = new ArrayList<RowDTO>();
					arrayList.add(rowDTO);
					comIDsDTOMap.put(rowDTO.getComId(), arrayList);
				}
			}
		}
		return comIDsDTOMap;
	}

	private void getPriceData(ReportDTO retReportDTO) {

		try {
			// get All COMIDS from input Rows
			Map<Long, List<RowDTO>> comIDsMap = getComIDsMap(retReportDTO.getBomDataDTO().getRowDTOList());
			Map<String, List<Map<String, String>>> result = new HashMap<String, List<Map<String, String>>>();
			ArrayList<Long> comIDs = new ArrayList<Long>();
			comIDs.addAll(comIDsMap.keySet());
			List<List<Long>> splitSets = MyPartition.partition(comIDs, PRICE_SERVICE_LIMIT);
			if (splitSets == null || splitSets.size() == 0) {
				retReportDTO.getErrorMessage().add(Constants.NO_COM_ID_RETURNED_FROM_BOM_SERVICE);
				return;
			}
			List<Future<Map<String, List<Map<String, String>>>>> futerList = new ArrayList<>();
			for (List<Long> distinctComIds : splitSets) {
				// result.putAll(callPriceWebService(distinctComIds));
				Future<Map<String, List<Map<String, String>>>> futureResult = priceAsyncClientService
						.callPriceWebService(distinctComIds);
				futerList.add(futureResult);
			}
			futerList.forEach(e -> {
				try {
					result.putAll(e.get());
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				} catch (ExecutionException e1) {
					e1.printStackTrace();
				}
			});
			mergePriceData(comIDsMap, result);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			logger.error(Constants.PRICE_SERVICE_LIMIT_NOT_FOUND_AT_PROPERTY);
			logger.error(e);
			retReportDTO.getErrorMessage().add(Constants.PRICE_SERVICE_LIMIT_NOT_FOUND_AT_PROPERTY);
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error(e);
			retReportDTO.getErrorMessage().add(e.getMessage());
		}
	}

	private void mergePriceData(Map<Long, List<RowDTO>> comIDsMap, Map<String, List<Map<String, String>>> result) {
		try {
			for (String comID : result.keySet()) {
				List<Map<String, String>> priceData = result.get(comID);
				Map<String, String> priceDataMap = priceData.get(0);
				List<RowDTO> rowDTOs = comIDsMap.get(Long.valueOf(comID));
				if (comIDsMap != null && rowDTOs != null) {
					for (RowDTO rowDTO : rowDTOs) {
						fillPriceData(rowDTO, priceDataMap);
					}
				} else {
					logger.error("comd_id was not found:" + comID);
				}

			}
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error(e);
		}

	}

	private void fillPriceData(RowDTO rowDTO, Map<String, String> priceDataMap) {

		PriceLeadReportSEDataDTO priceLeadReportSEDataDTO = new PriceLeadReportSEDataDTO();
		priceLeadReportSEDataDTO.setMaxLead(priceDataMap.get(Constants.MAXLEAD));
		priceLeadReportSEDataDTO.setMinLead(priceDataMap.get(Constants.MINLEAD));
		priceLeadReportSEDataDTO.setAvergePrice(priceDataMap.get(Constants.AVERGEPRICE));
		priceLeadReportSEDataDTO.setLastUpdateDate(priceDataMap.get(Constants.LASTUPDATEDATE));
		priceLeadReportSEDataDTO.setMinPrice(priceDataMap.get(Constants.MINPRICE));

		rowDTO.setReportsSEDataDTO(priceLeadReportSEDataDTO);

	}

	// uncheck because type safety Map.class
	/*
	 * @Async private Map<String, List<Map<String, String>>>
	 * callPriceWebService(List<Long> comIDs) {
	 * 
	 * Map<String, List<Map<String, String>>> retMap = new HashMap<>(); try {
	 * HttpHeaders headers = new HttpHeaders();
	 * headers.setContentType(MediaType.APPLICATION_JSON); Map<String, Object[]>
	 * inputs = new HashMap<>(); inputs.put("comIds", comIDs.toArray()); String json
	 * = new ObjectMapper().writeValueAsString(inputs); HttpEntity<String> entity =
	 * new HttpEntity<>(json, headers); retMap =
	 * restTemplate.postForEntity(PRICE_SERVICE_URL, entity, Map.class).getBody(); }
	 * catch (Throwable e) { e.printStackTrace(); logger.error(e); }
	 * 
	 * return retMap; }
	 */

}

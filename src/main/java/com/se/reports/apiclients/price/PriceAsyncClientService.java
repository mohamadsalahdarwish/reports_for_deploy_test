package com.se.reports.apiclients.price;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.se.reports.utils.Constants;
@Service
public class PriceAsyncClientService {
	final static Logger logger = Logger.getLogger(PriceAsyncClientService.class);
	

	@Autowired
	RestTemplate restTemplate;
	
	@Value(Constants.PRICE_SERVICE_URL)
	private String PRICE_SERVICE_URL;
	
	
	@Async
	public Future<Map<String, List<Map<String, String>>>> callPriceWebService(List<Long> comIDs) {

		logger.info("Thread : " + Thread.currentThread().getName());
		Map<String, List<Map<String, String>>> retMap = new HashMap<>();
		System.out.println("PRICE_SERVICE_URL"+PRICE_SERVICE_URL);
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			Map<String, Object[]> inputs = new HashMap<>();
			inputs.put("comIds", comIDs.toArray());
			String json = new ObjectMapper().writeValueAsString(inputs);
			HttpEntity<String> entity = new HttpEntity<>(json, headers);
			retMap = restTemplate.postForEntity(PRICE_SERVICE_URL, entity, Map.class).getBody();			
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error(e);
		}
		return new AsyncResult<Map<String,List<Map<String,String>>>>(retMap);
	}
}

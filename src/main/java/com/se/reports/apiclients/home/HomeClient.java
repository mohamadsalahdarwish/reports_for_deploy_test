package com.se.reports.apiclients.home;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.se.reports.apiclients.Client;
import com.se.reports.dto.UserDTO;
import com.se.reports.utils.Constants;

@Service
public class HomeClient extends Client {

	@Autowired
	RestTemplate restTemplate;

	@Value(Constants.HOME_SERVICE_URL)
	private String homeServiceUrl;
	
	final static Logger logger = Logger.getLogger(HomeClient.class);	

	public UserDTO callHomeUserService(long userId) {

		UserDTO userDTO = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			userDTO = restTemplate.getForEntity(homeServiceUrl + "/userAccount/users/" + userId, UserDTO.class, null, headers).getBody();
			
			logger.info("Successfully called the home user service, " + homeServiceUrl + "/userAccount/users/");
			
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error(e);
		}

		return userDTO;
	}
}
package com.se.reports.apiclients.bom;



import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.se.reports.apiclients.Client;
import com.se.reports.dto.BomDTO;
import com.se.reports.dto.ReportsSeviceParamsDTO;
import com.se.reports.utils.Constants;
/**
 * 
 * @author hany_shaker
 * use to Call bom Service API
 */
@Service
public class BOMAPIClient extends Client {
	
	@Value(Constants.WEBSERVICE_BOM_URL)
	private String BOM_WEBSERVICE_URL;
	
	@Autowired
	 RestTemplate restTemplate;
	
	final static Logger logger = Logger.getLogger(BOMAPIClient.class);
	
	public BomDTO getBomData(ReportsSeviceParamsDTO reportsSeviceParamsDTO ) {	
		long bomServiceCallingTime = System.currentTimeMillis();
		ResponseEntity<BomDTO> responseEntity = null;
		try {		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);		
		String json = new ObjectMapper().writeValueAsString(reportsSeviceParamsDTO);
		HttpEntity<String> entity = new HttpEntity<>(json, headers);
		responseEntity = restTemplate.postForEntity(BOM_WEBSERVICE_URL, entity, BomDTO.class);				
		logger.info(String.format(Constants.BOM_LOGGER_TIME, reportsSeviceParamsDTO.toJson(),(System.currentTimeMillis()-bomServiceCallingTime)));
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error(e);
			BomDTO errorBom = new BomDTO();
			errorBom.setErrorMessage(e.getMessage());
			return errorBom;
		}		
		return responseEntity.getBody();
	}
}


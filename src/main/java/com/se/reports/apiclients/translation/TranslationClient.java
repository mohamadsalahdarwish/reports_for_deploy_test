package com.se.reports.apiclients.translation;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.se.reports.apiclients.Client;
import com.se.reports.download.util.CaseInsensitiveMap;
import com.se.reports.dto.PriceLeadReportSEDataDTO;
import com.se.reports.dto.ReportDTO;
import com.se.reports.dto.RowDTO;
import com.se.reports.utils.Constants;
import com.se.reports.utils.MyPartition;

@Service
public class TranslationClient extends Client {

	@Autowired
	RestTemplate restTemplate;

	@Value(Constants.TRANSLATION_SERVICE_URL)
	private String translationServiceUrl;
	
	final static Logger logger = Logger.getLogger(TranslationClient.class);	

	public CaseInsensitiveMap callTranslationService(int language) {

		CaseInsensitiveMap retMap = new CaseInsensitiveMap();
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			String locale = getLanguageFileKey(language);
			Map<String, String> inputs = new HashMap<>();
			inputs.put("locale", locale);
			String json = new ObjectMapper().writeValueAsString(inputs);
			HttpEntity<String> entity = new HttpEntity<>(json, headers);
			retMap = restTemplate.postForEntity(translationServiceUrl + "/searchService/translation", entity, CaseInsensitiveMap.class).getBody();
			
			logger.info("Successfully called the translation service, " + translationServiceUrl + "/searchService/translation");
			
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error(e);
		}

		return retMap;
	}
	
	private static String getLanguageFileKey(int language)
	{
		String languageFileKey = "";
		switch (language)
		{
			case Constants.LANGUAGE_ENGLISH:
				languageFileKey = "en";
				break;
			case Constants.LANGUAGE_CHINESE:
				languageFileKey = "zh-cn";
				break;
			case Constants.LANGUAGE_TAIWANESE:
				languageFileKey = "zh-tw";
				break;
			default:
				languageFileKey = "en";
				break;
		}

		return languageFileKey;
	}
}

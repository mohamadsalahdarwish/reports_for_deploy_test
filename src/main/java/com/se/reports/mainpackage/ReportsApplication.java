package com.se.reports.mainpackage;

import java.util.Date;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.se.reports.utils.Constants;


@Configuration
@EnableAutoConfiguration
@SpringBootApplication
@ComponentScan("com.se.reports.*")
@EnableAsync
public class ReportsApplication extends SpringBootServletInitializer implements AsyncConfigurer {

	@Value(Constants.MAX_POOL_SIZE)
	private int MAX_POOL_SIZE;
	
	@Value(Constants.CORE_POOL_SIZE)
	private int CORE_POOL_SIZE;
	

	
	public static void main(String[] args) {
    	final Logger logger = Logger.getLogger(ReportsApplication.class);
        //Log starting App At logs
        logger.info(Constants.START_MESSAGE+new Date());
        SpringApplication springApplication = new SpringApplication(ReportsApplication.class);
        springApplication.run(args);

	}

   @Override
   protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
          return application.sources(ReportsApplication.class);
   }
	   
   @Bean(name = Constants.EXECUTER_NAME)
   @Override
   public Executor getAsyncExecutor()
   {
       ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
       taskExecutor.setMaxPoolSize(MAX_POOL_SIZE);
       taskExecutor.setCorePoolSize(CORE_POOL_SIZE);
       taskExecutor.setThreadNamePrefix(Constants.PRICE_SERVICE_THREAD_NAME);
       taskExecutor.initialize();

       return taskExecutor;
   }
   
}


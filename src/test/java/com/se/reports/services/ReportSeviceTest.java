package com.se.reports.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import com.se.reports.bussiness.LeadAndPriceReportService;
import com.se.reports.dto.ReportsSeviceParamsDTO;
import com.se.reports.mainpackage.ReportsApplication;

@Configuration
@SpringBootTest(classes = ReportsApplication.class)
//@SpringBootTest(classes = {ReportSevice.class, LeadAndPriceReportService.class})
@TestPropertySource(locations = {"file:D:/b001/app/configurations/se_reports_ws/reports.properties","file:/b001/app/configurations/se_reports_ws/reports.properties"})
public class ReportSeviceTest extends AbstractTestNGSpringContextTests {
	@Autowired(required=true)
	ReportSevice reportSevice;

	@Test
	void testGetPriceAndLeadReport() {
		try {
			long leadAndPriceServiceTime = System.currentTimeMillis();
			ReportsSeviceParamsDTO reportsSeviceParamsDTO = new ReportsSeviceParamsDTO(329, 9999364, 0, 1, 20);
			ResponseEntity responseReportDTO = reportSevice.getPriceAndLeadReport(reportsSeviceParamsDTO);
			System.out.println(
					"Lead & price Report Time :  " + (System.currentTimeMillis() - leadAndPriceServiceTime) + " Ms");
			System.err.println(responseReportDTO.getBody());
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}
	
	@Test
	void testDownloadPriceAndLeadReport() {
		try {
			long leadAndPriceServiceTime = System.currentTimeMillis();
//			16036/false/4/1000101/1
			ResponseEntity responseReportDTO = reportSevice.downloadPriceAndLeadReport(16036, false, 4, 1000101, 1, "qa-lb.siliconexpert.cn");
			System.out.println("Download Lead & price Report Time :  " + (System.currentTimeMillis() - leadAndPriceServiceTime) + " Ms");
			System.err.println(responseReportDTO.getBody());
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}

}

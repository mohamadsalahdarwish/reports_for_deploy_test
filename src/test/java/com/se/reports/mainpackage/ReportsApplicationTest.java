package com.se.reports.mainpackage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@EnableAutoConfiguration
@SpringBootApplication
@PropertySources({
		@PropertySource(value = "file:D:/b001/app/configurations/se_reports_ws/reports.properties", ignoreResourceNotFound = true),
		@PropertySource(value = "file:/b001/app/configurations/se_reports_ws/reports.properties", ignoreResourceNotFound = true) })
@ComponentScan("com.se.reports.*")
public class ReportsApplicationTest extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(ReportsApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ReportsApplication.class);
	}
}

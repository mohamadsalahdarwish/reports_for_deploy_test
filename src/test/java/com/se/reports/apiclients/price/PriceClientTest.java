package com.se.reports.apiclients.price;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import com.se.reports.apiclients.bom.BOMAPIClient;
import com.se.reports.apiclients.translation.TranslationClient;
import com.se.reports.bussiness.impl.LeadAndPriceReportServiceImpl;
import com.se.reports.download.excel.DownloadPriceAndLeadTimeReport;
import com.se.reports.dto.BomDTO;
import com.se.reports.dto.ReportDTO;
import com.se.reports.dto.ReportsSeviceParamsDTO;

@Configuration
@SpringBootTest(classes = {PriceClient.class, BOMAPIClient.class,LeadAndPriceReportServiceImpl.class,PriceAsyncClientService.class,DownloadPriceAndLeadTimeReport.class,TranslationClient.class})
@TestPropertySource(locations = {"file:D:/b001/app/configurations/se_reports_ws/reports.properties","file:/b001/app/configurations/se_reports_ws/reports.properties"})
public class PriceClientTest  extends AbstractTestNGSpringContextTests{

	@Autowired(required=true)
	BOMAPIClient bomapiClient;
	
	@Autowired
	PriceClient priceClient;
	
	@Autowired
	LeadAndPriceReportServiceImpl leadAndPriceReportService;
	
	
	
	@Test
	public void testCorrectPriceService() {
//		ReportsSeviceParamsDTO reportsSeviceParamsDTO = new ReportsSeviceParamsDTO(329, 9999364,0, 1, 500);
//		{"userId":1000101,"bomId":15954,"pageNumber":1,"pageSize":1000}
		ReportsSeviceParamsDTO reportsSeviceParamsDTO = new ReportsSeviceParamsDTO(1000101, 15954,0, 1, 100);
		BomDTO bomDTO = bomapiClient.getBomData(reportsSeviceParamsDTO);
		ReportDTO reportDTO = new ReportDTO();
		reportDTO.setBomDataDTO(bomDTO);
		ReportDTO retReportDTO = priceClient.getPriceLeadReportData(reportDTO);
		System.out.println(retReportDTO.toJson());
	}
}

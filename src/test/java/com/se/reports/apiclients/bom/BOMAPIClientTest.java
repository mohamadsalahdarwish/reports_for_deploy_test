package com.se.reports.apiclients.bom;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import com.se.reports.dto.BomDTO;
import com.se.reports.dto.ReportsSeviceParamsDTO;

@Configuration
@SpringBootTest(classes = BOMAPIClient.class)
//@TestPropertySource(value = "file:/b001/app/configurations/se_reports_ws/reports.properties")
@TestPropertySource(locations = {"file:D:/b001/app/configurations/se_reports_ws/reports.properties","file:/b001/app/configurations/se_reports_ws/reports.properties"})
 
public class BOMAPIClientTest extends AbstractTestNGSpringContextTests {
	@Autowired
	BOMAPIClient bomapiClient;
	
	@Test
	public void testCorrectBomService() {
		ReportsSeviceParamsDTO reportsSeviceParamsDTO = new ReportsSeviceParamsDTO(329, 9999364,0, 1, 50);
		BomDTO bomDTO = bomapiClient.getBomData(reportsSeviceParamsDTO);
		System.out.println(bomDTO.getRowDTOList().size());
	}
	
	@Test
	public void testBomValidation() {
		ReportsSeviceParamsDTO reportsSeviceParamsDTO = new ReportsSeviceParamsDTO(321, 9999364,0, 1, 50);
		BomDTO bomDTO = bomapiClient.getBomData(reportsSeviceParamsDTO);
		System.out.println(bomDTO.getErrorMessage());
	}
}

package com.se.reports.apiclients.translation;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import com.se.reports.apiclients.bom.BOMAPIClient;
import com.se.reports.bussiness.impl.LeadAndPriceReportServiceImpl;
import com.se.reports.dto.BomDTO;
import com.se.reports.dto.ReportDTO;
import com.se.reports.dto.ReportsSeviceParamsDTO;

@Configuration
@SpringBootTest(classes = {TranslationClient.class})
@TestPropertySource(locations = {"file:D:/b001/app/configurations/se_reports_ws/reports.properties","file:/b001/app/configurations/se_reports_ws/reports.properties"})
public class TranslationClientTest  extends AbstractTestNGSpringContextTests{

	@Autowired
	TranslationClient translationClient;
		
	@Test
	public void testCallTranslationService() {
		int language = 4;
		
		Map<String, String> retMap = translationClient.callTranslationService(language);
		
		System.out.println(retMap);
	}
}

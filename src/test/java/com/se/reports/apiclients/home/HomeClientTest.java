package com.se.reports.apiclients.home;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import com.se.reports.dto.UserDTO;

@Configuration
@SpringBootTest(classes = { HomeClient.class })
@TestPropertySource(locations = {
		"file:D:/b001/app/configurations/se_reports_ws/reports.properties",
		"file:/b001/app/configurations/se_reports_ws/reports.properties" })
public class HomeClientTest extends AbstractTestNGSpringContextTests {

	@Autowired
	HomeClient homeClient;

	@Test
	public void testCallHomeUserService() {
		int userId = 1000101;
		UserDTO userDTO = homeClient.callHomeUserService(userId);
		System.out.println(userDTO);
	}
}

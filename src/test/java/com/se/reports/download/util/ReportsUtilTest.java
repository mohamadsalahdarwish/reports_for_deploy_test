package com.se.reports.download.util;

import java.net.MalformedURLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import com.se.reports.apiclients.bom.BOMAPIClient;
import com.se.reports.bussiness.impl.LeadAndPriceReportServiceImpl;
import com.se.reports.dto.ReportsSeviceParamsDTO;
import com.se.reports.mainpackage.ReportsApplication;
import com.streaming.excel.silicon.excel.builder.template.SheetTemplate;

@ComponentScan("com.se.reports.*")
@SpringBootTest(classes = ReportsApplication.class)
@TestPropertySource(value = "file:D:/b001/app/configurations/se_reports_ws/reports.properties")
public class ReportsUtilTest extends AbstractTestNGSpringContextTests {	
	
	@Test
	public void testUpdateDomainName() throws MalformedURLException {
		String domain = "qa-lb.siliconexpert.cn";
//		String url = "http://download.siliconexpert.com/pdfs/2013/4/28/10/55/21/916/avx_/manual/188ccog.pdf";
		String url = "https://page.venkel.com/hubfs/Resources/Datasheets/C-Series.pdf";
		String result = ReportsUtil.updateUrlByHost(url, domain);
		System.out.println(result);	
	}	
}

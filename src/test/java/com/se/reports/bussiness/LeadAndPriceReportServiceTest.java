package com.se.reports.bussiness;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import com.se.reports.apiclients.bom.BOMAPIClient;
import com.se.reports.bussiness.impl.LeadAndPriceReportServiceImpl;
import com.se.reports.dto.ReportsSeviceParamsDTO;
import com.se.reports.mainpackage.ReportsApplication;
import com.streaming.excel.silicon.excel.builder.template.SheetTemplate;

@ComponentScan("com.se.reports.*")
@SpringBootTest(classes = ReportsApplication.class)
@TestPropertySource(value = "file:D:/b001/app/configurations/se_reports_ws/reports.properties")
public class LeadAndPriceReportServiceTest extends AbstractTestNGSpringContextTests {
	
	@Autowired
	LeadAndPriceReportServiceImpl leadAndPriceReportServiceImpl;	


	@Test
	public void testDownloadLeadAndPriceReport() {

//			{"userId":329,"bomId":9999364,"pageNumber":10,"pageSize":25}
//			16013/1/1/1000317/1
		ReportsSeviceParamsDTO reportsSeviceParamsDTO = new ReportsSeviceParamsDTO(1000317, 16013, 0, 10, 25, 4, "qa-lb.siliconexpert.cn");
		ResponseEntity<?> response = leadAndPriceReportServiceImpl.downloadReportData(reportsSeviceParamsDTO);
		System.out.println(response);
		
	
	}
}
